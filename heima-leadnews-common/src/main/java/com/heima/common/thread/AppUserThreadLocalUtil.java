package com.heima.common.thread;

import com.heima.model.user.pojo.ApUser;

/**
 * @author xujian
 * @date 2023/6/24 19:30
 */
public class AppUserThreadLocalUtil {
    private final static ThreadLocal<ApUser> AP_USER_THREAD_LOCAL=new ThreadLocal<>();
    public static void set(ApUser apUser){
        AP_USER_THREAD_LOCAL.set(apUser);
    }
    public static ApUser get(){
        return AP_USER_THREAD_LOCAL.get();
    }
    public static void clean(){
        AP_USER_THREAD_LOCAL.remove();
    }
}
