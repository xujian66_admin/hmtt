package com.heima.common.thread;

import lombok.Data;

/**
 * @author xujian
 * @date 2023/6/12 10:01
 */
@Data
public class UserInfo {
    private Long userId;
}
