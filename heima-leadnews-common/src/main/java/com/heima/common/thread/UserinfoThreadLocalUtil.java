package com.heima.common.thread;


/**
 * @author xujian
 * @date 2023/6/12 10:00
 */
public class UserinfoThreadLocalUtil {
    private final static ThreadLocal<UserInfo> WM_USER_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 设置用户
     * @param user
     */
    public static void setUser(UserInfo user){
        WM_USER_THREAD_LOCAL.set(user);
    }

    /**
     * 获取用户
     * @return
     */
    public static UserInfo getUser(){
        return WM_USER_THREAD_LOCAL.get();
    }

    /**
     * 清除用户
     */
    public static void clear(){
        WM_USER_THREAD_LOCAL.remove();
    }

}
