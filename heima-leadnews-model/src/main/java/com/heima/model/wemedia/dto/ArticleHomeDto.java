package com.heima.model.wemedia.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleHomeDto {
 // 最大时间
    Date maxBehotTime;
    // 最小时间
    Date minBehotTime;
    // 分页size
    Integer size;
    // 频道ID , 推荐频道 tab __all__ , 其他频道tag : 频道id
    String tag;
   }