package com.heima.model.wemedia.dto;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author xujian
 * @date 2023/6/25 11:51
 */
@Data
public class ChannelDto extends PageRequestDto {
    private String name;
}
