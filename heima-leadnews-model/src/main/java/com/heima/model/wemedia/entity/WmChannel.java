package com.heima.model.wemedia.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 自媒体频道表(WmChannel)表实体类
 *
 * @author makejava
 * @since 2023-06-14 09:21:48
 */
@Data
@SuppressWarnings("serial")
public class WmChannel  {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //频道名称
    private String name;
    //是否是默认频道;0: 否 1: 是
    private Integer isDefault;
    //频道顺序
    private Integer ord;
    //频道描述
    private String description;
    //频道状态;0:禁用 , 1 : 启用 , 2 : 删除
    private Integer status;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.UPDATE)
    private Date updatedTime;

}

