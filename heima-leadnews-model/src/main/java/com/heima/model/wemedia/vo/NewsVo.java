package com.heima.model.wemedia.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author xujian
 * @date 2023/6/25 19:02
 */
@Data
public class NewsVo {
    private Long id;
    private String title;
    private String images;
    private String content;
    private Long userId;
    private Long channelId;
    private Integer type;
    private Integer status;
    private String labels;
    private Integer enable;
    private String reason;
    private Date createdTime;
    private Date updatedTime;
    private Date publishTime;
    private Date submitedTime;
    private String authorName;
    private Long articleId;


}
