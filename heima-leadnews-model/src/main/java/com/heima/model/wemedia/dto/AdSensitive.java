package com.heima.model.wemedia.dto;

import com.heima.model.wemedia.entity.WmSensitive;
import lombok.Data;

/**
 * @author xujian
 * @date 2023/6/25 16:15
 */
@Data
public class AdSensitive extends WmSensitive {
}
