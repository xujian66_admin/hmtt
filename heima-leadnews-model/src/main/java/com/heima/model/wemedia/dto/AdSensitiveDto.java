package com.heima.model.wemedia.dto;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author xujian
 * @date 2023/6/25 11:27
 */
@Data
public class AdSensitiveDto extends PageRequestDto {
    private String name;

}
