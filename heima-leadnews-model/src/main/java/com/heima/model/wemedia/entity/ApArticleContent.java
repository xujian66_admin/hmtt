package com.heima.model.wemedia.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 文章内容信息表(ApArticleContent)表实体类
 *
 * @author Xujian
 * @since 2023-06-15 20:18:36
 */
@Data
@SuppressWarnings("serial")
public class ApArticleContent  {
    //文章id
    @TableId(value = "article_id",type = IdType.INPUT)
    private Long articleId;
    //文章内容
    private String content;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.UPDATE)
    private Date updatedTime;

}

