package com.heima.model.wemedia.dto;

import lombok.Data;

@Data
public class WmUserLoginDto {
    private String name;
    private String password;
}
