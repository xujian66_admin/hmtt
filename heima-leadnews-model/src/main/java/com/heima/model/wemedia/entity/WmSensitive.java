package com.heima.model.wemedia.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 自媒体敏感词表(WmSensitive)表实体类
 *
 * @author Xujian
 * @since 2023-06-15 19:31:06
 */
@Data
@SuppressWarnings("serial")
public class WmSensitive {
    //主键
    private Integer id;
    //敏感词
    private String sensitives;
    //创建时间
    @TableField(value = "CREATED_TIME", fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME", fill = FieldFill.UPDATE)
    private Date updatedTime;

}

