package com.heima.model.wemedia.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xujian
 * @date 2023/6/25 15:27
 */
@Data
public class AdChannel implements Serializable {
    private Integer id;
    private String name;
    private Integer ord;
    private String description;
    private Date createdTime;
    private Boolean isDefault;
    private Boolean status;
}
