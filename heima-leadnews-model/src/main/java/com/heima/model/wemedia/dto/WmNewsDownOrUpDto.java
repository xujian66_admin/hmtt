package com.heima.model.wemedia.dto;

import lombok.Data;

@Data
public class WmNewsDownOrUpDto {
    
    /**
    * 文章id
    */
    private Long id;
    /**
    * 是否上架  0 下架  1 上架
    */
    private Short enable;
}