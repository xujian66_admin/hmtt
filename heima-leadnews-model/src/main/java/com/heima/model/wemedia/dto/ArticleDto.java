package com.heima.model.wemedia.dto;

import com.heima.model.wemedia.entity.ApArticle;
import lombok.Data;

@Data
public class ArticleDto extends ApArticle {

    private String content ;
}