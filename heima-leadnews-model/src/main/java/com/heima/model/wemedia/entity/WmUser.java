package com.heima.model.wemedia.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 自媒体用户表(WmUser)表实体类
 *
 * @author makejava
 * @since 2022-06-15 11:59:51
 */
@SuppressWarnings("serial")
@TableName("wm_user")
@Data
public class WmUser {
    //主键
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    //用户名
    private String username;
    //盐
    private String salt;
    //密码
    private String password;
    //用户头像
    private String avatar;
    //手机号
    private String phone;
    //邮箱
    private String email;
    //昵称
    private String nickname;
    //位置
    private String location;
    //app端用户id
    private Long apUserId;
    //账号状态;0: 未启用  , 1:启用
    private Integer status;
    //账号类型 ; 1 普通账号 , 2 大V账号
    private Integer type;
    //创建时间
    private Date createdTime;
    //更新时间
    private Date updatedTime;

}
