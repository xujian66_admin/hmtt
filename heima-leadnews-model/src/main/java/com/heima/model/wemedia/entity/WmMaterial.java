package com.heima.model.wemedia.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.Date;

/**
 * @author xujian
 * @date 2023/6/12 10:48
 */
@TableName("wm_material")
@Data
public class WmMaterial {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    private Integer userId;
    private String url;
    private Integer type;
    private Integer isCollection;
    @TableField(value = "created_time",fill = FieldFill.INSERT)
    private Date createdTime;
    @TableField(value = "updated_time",fill = FieldFill.UPDATE)
    private Date updatedTime;
}
