package com.heima.model.wemedia.dto;

import com.heima.model.common.dtos.PageRequestDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WmMaterialDto extends PageRequestDto {

    /**
     * 1 收藏
     * 0 
     */
    @ApiModelProperty(value = "是否收藏",required = false)
    private Short isCollection;
}