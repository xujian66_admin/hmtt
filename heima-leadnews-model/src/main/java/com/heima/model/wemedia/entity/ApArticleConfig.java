package com.heima.model.wemedia.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 文章配置表(ApArticleConfig)表实体类
 *
 * @author Xujian
 * @since 2023-06-15 20:18:36
 */
@Data
@SuppressWarnings("serial")
public class ApArticleConfig  {
    //文章id
    @TableId(value = "article_id",type = IdType.INPUT)
    private Long articleId;
    //是否上架
    private Integer enable;
    //是否删除
    private Integer isDelete;
    //是否允许评论
    private Integer isComment;
    //是否允许转发
    private Integer isForward;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.UPDATE)
    private Date updatedTime;

}

