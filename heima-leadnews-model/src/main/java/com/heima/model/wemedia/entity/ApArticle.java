package com.heima.model.wemedia.entity;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 文章基础信息表(ApArticle)表实体类
 *
 * @author Xujian
 * @since 2023-06-15 20:18:34
 */
@Data
@SuppressWarnings("serial")
public class ApArticle  {
    //主键
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    //自媒体端文章ID
    private Long newsId;
    //文章标题
    private String title;
    //文章封面图片
    private String images;
    //文章作者ID
    private Long authorId;
    //作者名称
    private String authorName;
    //文章频道ID
    private Long channelId;
    //频道名称
    private String channelName;
    //文章布局类型;0 : 无图 1 :单图 3:多图
    private Integer layout;
    //文章标签
    private String labels;
    //文章发布时间
    private Date publishTime;
    //点赞数量
    private Integer likes;
    //评论书数量
    private Integer comment;
    //阅读数量
    private Integer views;
    //收藏数量
    private Integer collection;

    //静态页面地址
    private String staticUrl;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.UPDATE)
    private Date updatedTime;

}

