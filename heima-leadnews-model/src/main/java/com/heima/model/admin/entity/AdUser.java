package com.heima.model.admin.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 管理员用户信息表(AdUser)实体类
 *
 * @author makejava
 * @since 2022-04-21 21:42:12
 */
@Data
public class AdUser implements Serializable {
    private static final long serialVersionUID = 833203261026267801L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 登录用户名
     */
    private String username;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 盐
     */
    private String salt;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像
     */
    private String image;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 状态
     * 0 暂时不可用
     * 1 永久不可用
     * 9 正常可用
     */
    private Integer status;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 最后一次登录时间
     */
    private Date loginTime;
    /**
     * 创建时间
     */
    private Date createdTime;

}