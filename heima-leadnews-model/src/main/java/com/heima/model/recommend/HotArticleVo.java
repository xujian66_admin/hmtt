package com.heima.model.recommend;

import com.heima.model.wemedia.entity.ApArticle;
import lombok.Data;

@Data
public class HotArticleVo extends ApArticle {
    /**
     * 文章分值
     */
    private Integer score;
}