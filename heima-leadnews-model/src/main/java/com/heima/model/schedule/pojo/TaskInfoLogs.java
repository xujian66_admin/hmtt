package com.heima.model.schedule.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * (TaskInfoLogs)表实体类
 *
 * @author Xujian
 * @since 2023-06-18 10:24:53
 */
@Data
@SuppressWarnings("serial")
public class TaskInfoLogs  {
    //任务id
    @TableId(value = "task_id",type = IdType.INPUT)
    private Long taskId;
    //执行时间
    private Date executeTime;
    //参数
    private Byte[] parameters;
    //优先级
    private Integer priority;
    //任务类型
    private Integer taskType;
    //版本号,用乐观锁
    private Integer version;
    //状态 0=初始化状态 1=EXECUTED 2=CANCELLED
    private Integer status;

}

