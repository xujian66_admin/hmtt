package com.heima.model.schedule.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * (TaskInfo)表实体类
 *
 * @author Xujian
 * @since 2023-06-18 10:24:52
 */
@Data
@SuppressWarnings("serial")
public class TaskInfo  {
    //任务id
    @TableId(value = "task_id",type = IdType.ASSIGN_ID)
    private Long taskId;
    //执行时间
    private Date executeTime;
    //参数
    private byte[] parameters;
    //优先级
    private Integer priority;
    //任务类型
    private Integer taskType;
    //加载表示
    private Integer isLoad;

}

