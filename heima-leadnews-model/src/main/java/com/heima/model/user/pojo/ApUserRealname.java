package com.heima.model.user.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * APP实名认证信息表(ApUserRealname)表实体类
 *
 * @author Xujian
 * @since 2023-06-26 16:27:21
 */
@Data
@TableName(value = "ap_user_realname")
@SuppressWarnings("serial")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApUserRealname  {

    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //账号ID
    private Long userId;
    //用户名称
    private String name;
    //资源名称
    private String idno;
    //正面照片
    private String fontImage;
    //背面照片
    private String backImage;
    //手持照片
    private String holdImage;
    //活体照片
    private String liveImage;
    /*状态
;           0 创建中
            1 待审核
            2 审核失败
            9 审核通过
     */
    private Integer status;
    //拒绝原因
    private String reason;
    //创建时间
    private Date createdTime;
    //更新时间
    private Date updatedTime;

}

