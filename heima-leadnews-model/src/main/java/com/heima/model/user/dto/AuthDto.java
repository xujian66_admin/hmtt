package com.heima.model.user.dto;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

@Data
public class AuthDto extends PageRequestDto {

    /**
    * 状态
    */
    private Short status;
    /**
     * 需要进行实名认证审核的id
     */
    private Long id;

    /**
     * 审核失败的原因
     */
    private String msg;
}