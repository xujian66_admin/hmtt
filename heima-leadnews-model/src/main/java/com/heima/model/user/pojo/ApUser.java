package com.heima.model.user.pojo;

import java.util.Date;

import lombok.Data;

/**
 * APP用户表(ApUser)表实体类
 *
 * @author Xujian
 * @since 2023-06-23 17:56:57
 */
@Data
@SuppressWarnings("serial")
public class ApUser  {
    //主键
    private Long id;
    //用户名
    private String username;
    //密码
    private String password;
    //盐
    private String salt;
    //昵称
    private String nicknme;
    //头像
    private String image;
    //手机号
    private String phone;
    //性别;0: 男  1 :女
    private Integer gender;
    //状态;0 : 正常 , 1 : 封禁 , 2 : 删除
    private Integer status;
    //标签;0 : 普通用户 , 1 : 大v用户  2 : 企业用户
    private Integer flag;
    //是否认证;0: 未认证  1 : 已认证
    private Integer isAuthentication;
    //创建时间
    private Date createdTime;
    //更新时间
    private Date updatedTime;

}

