package com.heima.model.common.constants;

/**
 * @author xujian
 * @date 2023/6/17 10:59
 */
public class KafkaMessageConstants {
    /**
     * 自媒体文章自动审核主题
     */
    public static final String WM_NEWS_AUTO_SCAN_TOPIC = "wm.news.auto.scan.topic";
    public static final String WM_NEWS_UP_DOWN_TOPIC = "wm.news.up.down.topic";
    public static final String AP_ARTICLE_ES_SYNC_TOPIC = "ap.article.es.sync.topic";
    /**
     * 用户行为数据采集topic
     */
    public static final String HOT_ARTICLE_BEHAVIOR_TOPIC_INPUT = "hot.article.behavior.topic.input";

    /**
     * 行为数据聚合结果输出的topic
     */
    public static final String HOT_ARTICLE_BEHAVIOR_TOPIC_OUTPUT = "hot.article.behavior.topic.output";
}
