package com.heima.model.common.constants;

public class ArticleContants {

    public static final short LOAD_TYPE_MORE = 1;

    public static final short LOAD_TYPE_NEW = 2;

    public static final String DEFAULT_TAG = "__all__";

    public static final Integer HOT_ARTICLE_LIKE_WEIGHT = 3;

    public static final Integer HOT_ARTICLE_COMMENT_WEIGHT = 5;

    public static final Integer HOT_ARTICLE_VIEW_WEIGHT = 1;

    public static final Integer HOT_ARTICLE_COLLECT_WEIGHT = 8;

    public static final String  HOT_ARTICLE_PREFIX = "HOT_ARTICLE";

    public static final String  HOT_ARTICLE_INFO_KEY = "HOT_ARTICLE_INFO";
}