package com.heima.schedule.lock;

import cn.hutool.core.lang.UUID;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ThreadUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author xujian
 * @date 2023/6/20 16:45
 */
@Slf4j
@Service
public class RedisLockImpl implements RedisLock{

    private StringRedisTemplate stringRedisTemplate;

    ThreadLocal<String> t1=new ThreadLocal<>();

    public RedisLockImpl(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Override
    public boolean addLock(Long await,String key, long timeout, TimeUnit unit) {

        String uuid = UUID.randomUUID().toString();
        t1.set(uuid);
        Boolean ifAbsent = stringRedisTemplate.opsForValue().setIfAbsent(key, uuid, timeout, unit);
        log.info("获取分布式锁,获取结果:{}", ifAbsent ? "成功" : "失败");
        if (!ifAbsent) {
            //记录已经等待的时间
            Long waited = 0L;
            int count = 0;
            log.info("获取分布式锁失败,开始进行阻塞重试");
            while (await > waited) {
                try {
                    Thread.sleep(100);
                    //已经等待时间累加
                    waited += 100;
                    count++;
                    log.info("获取分布式锁失败,开始进行阻塞重试,重试次数:{},已等待时间:{}", count, waited);
                    ifAbsent = stringRedisTemplate.opsForValue().setIfAbsent(key, uuid, timeout, unit);
                    if (ifAbsent) {
                        log.info("重试获取分布式锁成功,重试次数:{},已等待时间:{}", count, waited);
                        break;
                    }
                } catch (Exception e) {
                    log.error("分布式锁重试失败,重试次数:{}", count);
                }
            }

        }
        if (ifAbsent) {
            new Thread(new UpdateLockTimeoutTask(stringRedisTemplate, uuid, timeout, unit, key)).start();
        }

        return ifAbsent;

    }

    @Override
    public void unlock(String key) {
        String uuid = stringRedisTemplate.opsForValue().get(key);
        if (!StringUtils.equals(uuid,t1.get())){
            return;
        }
        stringRedisTemplate.delete(key);
        //清空threadLocal,防止内存泄露
        t1.remove();
        //终止续命线程

        //获取续命线程ID
        String threadId = stringRedisTemplate.opsForValue().get(uuid);
        if (!StringUtils.isEmpty(threadId)){
            //根据线程ID获取线程对象
            Thread thread = ThreadUtils.findThreadById(Long.valueOf(threadId));
            if (thread!=null){
                //终止线程
                thread.interrupt();
            }
        }
    }
}
