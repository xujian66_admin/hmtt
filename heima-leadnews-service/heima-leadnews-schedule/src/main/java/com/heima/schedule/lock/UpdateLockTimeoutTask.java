package com.heima.schedule.lock;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @Author Administrator
 * @Date 2023/6/20
 **/
@Slf4j
public class UpdateLockTimeoutTask implements Runnable{

    private StringRedisTemplate stringRedisTemplate;
    private String uuid;
    private long timeout ;
    private TimeUnit unit;
    private String key;

    public UpdateLockTimeoutTask(StringRedisTemplate stringRedisTemplate, String uuid, long timeout, TimeUnit unit, String key) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.uuid = uuid;
        this.timeout = timeout;
        this.unit = unit;
        this.key = key;
    }

    @Override
    public void run() {
        //获取续命线程ID
        long threadId = Thread.currentThread().getId();
        //获取线程状态(是否是中断状态)
        boolean interrupted = Thread.currentThread().isInterrupted();
        log.info("是否被回收:{}",interrupted);
        //如果线程未中断 , 执行续命逻辑
        while (!interrupted){
            try {
                //续命休眠等待时间 , 一般是分布式过期时间的1/3
                Thread.sleep(unit.toMillis(timeout)/3);
                //以线程唯一标识uuid作为key , 线程真实ID作为value , 保存到Redis中(方便在释放锁的时候获取真实ID)
                stringRedisTemplate.opsForValue().set(uuid,threadId+"",timeout, unit);
                //刷新分布式锁过期时间
                stringRedisTemplate.expire(key, timeout, unit);
            } catch (Exception e) {
                log.info("休眠中断 , 分布式锁续命线程终止------");
                Thread.currentThread().interrupt();
            }
        }
    }
}