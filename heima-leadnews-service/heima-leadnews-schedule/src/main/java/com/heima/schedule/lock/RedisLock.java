package com.heima.schedule.lock;

import java.util.concurrent.TimeUnit;

/**
 * @author xujian
 * @date 2023/6/20 16:42
 */
public interface RedisLock {
    /**
     * 加锁接口
     * @param key
     * @param timeout
     * @param unit
     * @return
     */
    boolean addLock(Long await,String key, long timeout, TimeUnit unit);

    /**
     * 释放锁操作
     * @param key
     */
    void unlock(String key);
}
