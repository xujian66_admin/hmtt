package com.heima.schedule;

import com.hei.feign.client.ApArticleFeignClient;
import com.hei.feign.client.TaskFeignClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author xujian
 * @date 2023/6/18 10:12
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@MapperScan("com.heima.schedule.mapper")

public class ScheduleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScheduleApplication.class,args);
    }
}
