package com.heima.schedule.config;

import com.heima.common.exception.ExceptionCatch;
import com.heima.common.swagger.SwaggerConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author xu
 */
@Configuration
@Import({SwaggerConfiguration.class, ExceptionCatch.class})
public class InitConfig {
}