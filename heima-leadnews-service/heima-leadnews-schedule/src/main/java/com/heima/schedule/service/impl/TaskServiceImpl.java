package com.heima.schedule.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.heima.common.exception.CustomException;
import com.heima.model.common.constants.ScheduleConstants;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.schedule.pojo.TaskInfo;
import com.heima.model.schedule.pojo.TaskInfoLogs;
import com.heima.schedule.mapper.TaskInfoLogsMapper;
import com.heima.schedule.mapper.TaskInfoMapper;
import com.heima.schedule.service.TaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author xujian
 * @date 2023/6/18 10:51
 */

@Service
public class TaskServiceImpl implements TaskService {

    @Resource
    TaskInfoMapper taskInfoMapper;
    @Resource
    TaskInfoLogsMapper taskInfoLogsMapper;
    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Override
    public long addTask(TaskDto task) {
        //校验参数
        if (task==null||task.getTaskType()==null||task.getPriority()==null||task.getExecuteTime()<=0){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //保存任务数据到DB
        TaskInfo taskInfo=addTaskToDb(task);
        //保存任务数据到redis
        if (taskInfo!=null) {
            Integer isLoad = addTaskToRedis(taskInfo);
            if (isLoad==1) {
                taskInfo.setIsLoad(1);
                taskInfoMapper.updateById(taskInfo);
            }
        }
                return taskInfo.getTaskId();
    }

    @Override
    public boolean cancelTask(Long taskId) {
        //校验参数
        if (taskId==null) {
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //从数据库删除数据
        TaskInfo taskInfo=removeTaskFromDb(taskId);
        //从redis中删除数据
        if (taskInfo!=null){
            return removeTaskFromRedis(taskInfo);
        }
        return false;
    }

    @Override
    public TaskInfo pollTask(Integer taskType, Integer priority) {
        if (taskType==null||priority==null) {
            throw  new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        String taskId=polltaskFromRedis(taskType,priority);
        if (StringUtils.isEmpty(taskId)) {
            return null;
        }
        return getAndRemoveTaskInfo(taskId);
    }

    private TaskInfo getAndRemoveTaskInfo(String taskId) {
        TaskInfo taskInfo = taskInfoMapper.selectById(taskId);
        if (taskInfo==null) {
            return null;
        }
        //从数据库中删除数据修改日志状态
        taskInfoMapper.deleteById(taskId);
        TaskInfoLogs taskInfoLogs = taskInfoLogsMapper.selectById(taskId);
        taskInfoLogs.setStatus(ScheduleConstants.EXECUTED);
        taskInfoLogsMapper.updateById(taskInfoLogs);
        return taskInfo;
    }

    private String polltaskFromRedis(Integer taskType, Integer priority) {
        String key=ScheduleConstants.TASK_TOPIC+taskType+"_"+priority;
        String taskId=stringRedisTemplate.opsForList().rightPop(key);
        return taskId;
    }

    /**
     * 从redis中删除数据
     * @param taskInfo
     * @return
     */
    private boolean removeTaskFromRedis(TaskInfo taskInfo) {
        //获取任务执行时间
        long executeTime = taskInfo.getExecuteTime().getTime();
        //获取预加载时间（5min）
        long preloadTime=DateUtil.offsetMinute(new Date(),5).getTime();
        //任务执行时间小于等于当前时间（任务需要立刻执行），从redis中List集合删除数据
        if (executeTime<=System.currentTimeMillis()) {
            //List集合Key规则 前缀+任务类型+优先级
            String key=ScheduleConstants.TASK_TOPIC+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForList().remove(key,1,taskInfo.getTaskId().toString());
        }else if(executeTime<=preloadTime){
            //任务执行时间大于当前时间小于预加载时间（任务即将执行）,从Redis中ZSET集合删除任务
            //ZSET集合 可以规则：前缀+任务类型+优先级
            String key=ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForZSet().remove(key,taskInfo.getTaskId().toString());
        }
        return true;
    }

    /**
     * 从数据库删除task数据
     * @param taskId
     * @return
     */
    private TaskInfo removeTaskFromDb(Long taskId) {
        //根据id判断是否存在数据
        TaskInfo taskInfo = taskInfoMapper.selectById(taskId);
        if (taskInfo==null){
            throw new CustomException(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        taskInfoMapper.deleteById(taskId);
        //修改task_info_logs中任务状态为已取消
        TaskInfoLogs taskInfoLogs=taskInfoLogsMapper.selectById(taskId);
        taskInfoLogs.setStatus(ScheduleConstants.CANCELLED);
        taskInfoLogsMapper.updateById(taskInfoLogs);
        return taskInfo;
    }

    /**
     * 保存到redis
     * @param taskInfo
     */
    private Integer addTaskToRedis(TaskInfo taskInfo) {
        Integer isLoad =0;
        //获取任务执行时间
        long executeTime = taskInfo.getExecuteTime().getTime();
        //获取预加载时间
        long preloadTime = DateUtil.offsetMinute(new Date(), 5).getTime();
        //任务执行时间小于等于当前时间（任务需要立刻执行） ，添加任务数据到Redis中的List集合
        if(executeTime<=System.currentTimeMillis()){
            String key=ScheduleConstants.TASK_TOPIC+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForList().leftPush(key,taskInfo.getTaskId().toString());

            isLoad=1;
        }else if(executeTime<=preloadTime){
            //任务执行时间大于当前时间小于预加载时间（任务即将执行）从redis中ZSET集合删除任务
            //ZSET集合 key规则：前缀+任务类型+优先级
            String key =ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForZSet().add(key,taskInfo.getTaskId().toString(),executeTime);

            isLoad=1;
        }
        return isLoad;
    }

    /**
     * 保存任务数据到数据库
     * @param task
     * @return
     */
    private TaskInfo addTaskToDb(TaskDto task) {
        //保存任务数据到TaskInfo表
        TaskInfo taskInfo=new TaskInfo();
        BeanUtil.copyProperties(task,taskInfo);
        taskInfo.setExecuteTime(new Date(task.getExecuteTime()));
        taskInfoMapper.insert(taskInfo);

        //保存数据到TaskInfoLog表
        TaskInfoLogs taskInfoLogs=new TaskInfoLogs();
        BeanUtil.copyProperties(taskInfo,taskInfoLogs);
        taskInfoLogs.setVersion(0);
        taskInfoLogs.setStatus(ScheduleConstants.SCHEDULED);
        taskInfoLogsMapper.insert(taskInfoLogs);
        return taskInfo;

    }
}
