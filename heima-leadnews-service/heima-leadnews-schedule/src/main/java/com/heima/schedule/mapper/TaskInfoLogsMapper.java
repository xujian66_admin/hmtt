package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.schedule.pojo.TaskInfoLogs;

/**
 * (TaskInfoLogs)表数据库访问层
 *
 * @author Xujian
 * @since 2023-06-18 10:24:53
 */
public interface TaskInfoLogsMapper extends BaseMapper<TaskInfoLogs> {

}

