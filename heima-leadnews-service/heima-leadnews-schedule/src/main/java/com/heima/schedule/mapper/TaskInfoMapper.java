package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.schedule.pojo.TaskInfo;

/**
 * (TaskInfo)表数据库访问层
 *
 * @author Xujian
 * @since 2023-06-18 10:24:51
 */
public interface TaskInfoMapper extends BaseMapper<TaskInfo> {

}

