package com.heima.schedule.service;

import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.schedule.pojo.TaskInfo;
import org.springframework.transaction.annotation.Transactional;

/**
 * 对外访问接口
 */
public interface TaskService {


    /**
     * 添加任务
     * @param task   任务对象
     * @return       任务id
     */
    public long addTask(TaskDto task) ;

    /**
     * 取消任务
     * @param taskId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean cancelTask(Long taskId);

    /**
     * 拉取消费
     * @param taskType
     * @param priority
     * @return
     */
    public TaskInfo pollTask(Integer taskType,Integer priority);
}