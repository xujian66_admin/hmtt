package com.heima.schedule.task;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.model.common.constants.ScheduleConstants;
import com.heima.model.schedule.pojo.TaskInfo;
import com.heima.schedule.lock.RedisLock;
import com.heima.schedule.mapper.TaskInfoMapper;
import com.heima.schedule.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author xujian
 * @date 2023/6/18 19:58
 */
@Component
@Slf4j
public class ScheduleTask {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private TaskInfoMapper taskInfoMapper;
    @Resource
    private RedisLock redisLock;


    //@Scheduled(cron = "0/2 * * * * ?")
    @Scheduled(fixedDelay = 3000)
    public void refresh(){
        boolean refresh_key = redisLock.addLock(2000L,"refresh_key", 2, TimeUnit.MINUTES);

        if (!refresh_key){
            return;
        }
        try {
            //找到系统中存储任务的预加载集合 task_future_
            Set<String> keys = redisUtils.scan("task_future_*");
            //遍历获取每一个预加载集合
            for (String futureKey : keys) {
                //从预加载集合中获取到了执行时间的任务 任务执行时间<=当前时间
                Set<String> taskIds = stringRedisTemplate.opsForZSet().rangeByScore(futureKey, 0, System.currentTimeMillis());
                if (CollectionUtils.isEmpty(taskIds)) {
                    continue;
                }
                //将加载到了时间的任务存入List集合
                String topicKey = futureKey.replace(ScheduleConstants.TASK_FUTURE, ScheduleConstants.TASK_TOPIC);
                redisUtils.refreshWithPipeline(futureKey,topicKey,taskIds);
            }

        } finally {
            log.info("释放分布式锁------------------");
            redisLock.unlock("refresh_key");
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void loadTask(){
        //查询数据库中已经到了预加载时间但是没有被加载的数据
        log.info("----开始加载数据到Redis！-----");
        DateTime preloadTime = DateUtil.offsetMinute(new Date(), 5);
        LambdaQueryWrapper<TaskInfo> wrapper = Wrappers.<TaskInfo>lambdaQuery().eq(TaskInfo::getIsLoad, 0).le(TaskInfo::getExecuteTime, preloadTime);
        List<TaskInfo> taskInfoList = taskInfoMapper.selectList(wrapper);
        log.info("查询到数据库中需要加载的任务：{}",taskInfoList.stream().map(taskInfo -> taskInfo.getTaskId()).collect(Collectors.toList()));
        // 将任务数据保存到Redis中
        if (CollectionUtils.isEmpty(taskInfoList)) {
            return;
        }
        //2.将任务数据保存到Redis
        for (TaskInfo taskInfo : taskInfoList) {
            log.info("---开始加载任务任务信息{}----",taskInfo);
            String key=ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForZSet().add(key,taskInfo.getTaskId()+"",taskInfo.getExecuteTime().getTime());
            //更新数据库加载状态
            taskInfo.setIsLoad(1);
            taskInfoMapper.updateById(taskInfo);
        }
        log.info("----数据加载完毕！-----");

    }
}
