package com.heima.schedule.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.schedule.pojo.TaskInfo;
import com.heima.schedule.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
*
*@author xujian
*@date 2023/6/21 9:22
*
*/
@RestController
@Slf4j
@RequestMapping("/api/v1/task")
public class TaskController {

    @Resource
    private TaskService taskService;

    @PostMapping("/add")
    public ResponseResult<Long> addTask(@RequestBody TaskDto taskDto){
        long taskId = taskService.addTask(taskDto);
        return ResponseResult.okResult(taskId);
    }

    @DeleteMapping("/{taskId}")
    public ResponseResult<Boolean> cancelTask(@PathVariable("taskId") Long taskId){
        boolean flag = taskService.cancelTask(taskId);
        return ResponseResult.okResult(flag);
    }

    @GetMapping("/poll/{taskType}/{priority}")
    public ResponseResult<TaskInfo> pollTask(@PathVariable("taskType") Integer taskType, @PathVariable("priority") Integer priority){
        TaskInfo taskInfo = taskService.pollTask(taskType, priority);
        return ResponseResult.okResult(taskInfo);
    }

}
