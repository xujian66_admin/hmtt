package com.heima.schedule.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.heima.model.schedule.dto.TaskDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author xujian
 * @date 2023/6/18 20:59
 */
@SpringBootTest
class TaskServiceTest {

    @Resource
    private TaskService taskService;
    @Test
    void addTask(){
        for (int i = 0; i < 10; i++) {
            TaskDto taskDto=new TaskDto();
            taskDto.setTaskType(RandomUtil.randomInt(0,2));
            taskDto.setPriority(RandomUtil.randomInt(0,2));
            taskDto.setExecuteTime(DateUtil.offsetMinute(new Date(),RandomUtil.randomInt(0,10)).getTime());
            taskDto.setParameters("aa".getBytes(StandardCharsets.UTF_8));
            Long taskId= taskService.addTask(taskDto);
            System.out.println(taskId);
        }
    }
}