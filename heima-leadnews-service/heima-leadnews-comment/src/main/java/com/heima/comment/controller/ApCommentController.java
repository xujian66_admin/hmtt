package com.heima.comment.controller;

import com.heima.comment.dto.CommentLikeDto;
import com.heima.comment.dto.CommentListDto;
import com.heima.comment.dto.CommentSaveDto;
import com.heima.comment.service.ApCommentService;
import com.heima.comment.vo.ApCommentVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author xujian
 * @date 2023/6/27 15:50
 */
@RestController
@RequestMapping(path = "/api/v1/comment")
public class ApCommentController {

    @Resource
    private ApCommentService apCommentService;

    /**
     * 评论—多类型保存接口
     * @param dto
     * @return
     */
    @PostMapping(path = "/save")
    public ResponseResult save(@RequestBody CommentSaveDto dto){
        if (StringUtils.isEmpty(dto.getContent())||dto.getArticleId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        return apCommentService.saveComment(dto.getContent(),dto.getArticleId().toString());
    }

    @PostMapping(path = "/like")
    public ResponseResult like(@RequestBody CommentLikeDto dto){
        if (StringUtils.isEmpty(dto.getCommentId())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getOperation()!=0&&dto.getOperation()!=1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        return apCommentService.like(dto.getCommentId(),dto.getOperation());
    }

    @PostMapping(path = "/load")
    public ResponseResult<List<ApCommentVo>> load(@RequestBody CommentListDto dto){
        if (dto.getArticleId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //最小时间校验
        Date minTime=dto.getMinDate()==null?new Date():dto.getMinDate();
        //每页展示数据条数校验
        Integer size =dto.getSize()==null||dto.getSize()<=0?10: dto.getSize();
        size=Math.min(size,50);
        return apCommentService.load(dto.getArticleId().toString(),minTime,size);
    }

}
