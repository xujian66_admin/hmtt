package com.heima.comment.controller;

import com.heima.comment.dto.*;
import com.heima.comment.service.ApCommentService;
import com.heima.comment.vo.ApCommentVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author Administrator
 * @Date 2023/6/27
 **/
@RestController
@RequestMapping(path = "/api/v1/comment_repay")
public class CommentRepayController {


    @Resource
    private ApCommentService apCommentService;


    /**
     * 发表评论回复
     *
     * @param dto
     * @return
     */
    @PostMapping(path = "/save")
    @ApiOperation("发表评论回复")
    public ResponseResult save(@RequestBody CommentRepayDto dto) {
        if (StringUtils.isEmpty(dto.getContent()) || dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        return apCommentService.saveCommentRepay(dto.getCommentId().toString(), dto.getContent());
    }
    /**
     * 评论回复点赞
     *
     * @param dto
     * @return
     */
    @PostMapping(path = "/like")
    @ApiOperation("评论回复点赞")
    public ResponseResult like(@RequestBody CommentRepayLikeDto dto) {
        if (StringUtils.isEmpty(dto.getCommentRepayId())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getOperation() != 0 && dto.getOperation() != 1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        return apCommentService.like(dto.getCommentRepayId(), dto.getOperation());
    }

    /**
     * 查询评论回复列表
     *
     * @param dto
     * @return
     */
    @PostMapping(path = "/load")
    @ApiOperation("查询评论回复列表")
    public ResponseResult<List<ApCommentVo>> load(@RequestBody CommentRepayListDto dto) {
        if (dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //最小时间校验
        Date minTime = dto.getMinDate() == null ? new Date() : dto.getMinDate();
        //每页展示数据条数校验
        Integer size = dto.getSize() == null || dto.getSize() <= 0 ? 10 : dto.getSize();
        size = Math.min(size, 50);

        return apCommentService.load(dto.getCommentId().toString(), minTime, size);
    }
}
