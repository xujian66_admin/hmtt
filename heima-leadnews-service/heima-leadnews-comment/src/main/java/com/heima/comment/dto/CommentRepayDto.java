package com.heima.comment.dto;

import lombok.Data;

@Data
public class CommentRepayDto {

   /**
    * 评论id
    */
   private String commentId;

   /**
    * 评论内容
    */
   private String content;
}