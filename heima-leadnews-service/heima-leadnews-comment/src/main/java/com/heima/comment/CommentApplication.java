package com.heima.comment;

import com.hei.feign.client.UserFeignClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author xujian
 * @date 2023/6/27 15:12
 */
@SpringBootApplication
@EnableFeignClients(clients = UserFeignClient.class)
public class CommentApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommentApplication.class,args);
    }
}
