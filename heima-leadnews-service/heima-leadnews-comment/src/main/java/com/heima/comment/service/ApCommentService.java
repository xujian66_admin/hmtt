package com.heima.comment.service;

import com.heima.comment.vo.ApCommentVo;
import com.heima.model.common.dtos.ResponseResult;

import java.util.Date;
import java.util.List;

/**
 * @author xujian
 * @date 2023/6/27 15:52
 */
public interface ApCommentService {
    /**
     *
     * @param content 评论的内容
     * @param targetId 目标类型id
     * @return
     */
    ResponseResult saveComment(String content, String targetId);


    ResponseResult like(String targetId,Short operation);

    ResponseResult<List<ApCommentVo>> load(String targetId, Date minTime,Integer size);
    /**
     * 发布评论回复
     * @param toString
     * @param content
     * @return
     */
    ResponseResult saveCommentRepay(String toString, String content);
}
