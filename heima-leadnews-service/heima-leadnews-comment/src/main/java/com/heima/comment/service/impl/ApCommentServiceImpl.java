package com.heima.comment.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.hei.feign.client.UserFeignClient;
import com.heima.comment.pojo.ApComment;
import com.heima.comment.pojo.ApCommentLike;
import com.heima.comment.service.ApCommentService;
import com.heima.comment.vo.ApCommentVo;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserinfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojo.ApUser;
import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xujian
 * @date 2023/6/27 15:54
 */
@Service
public class ApCommentServiceImpl implements ApCommentService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private UserFeignClient userFeignClient;
    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public ResponseResult saveComment(String content, String targetId ) {
        //校验用户登录状态
        UserInfo user = UserinfoThreadLocalUtil.getUser();
        if (user==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        String user_info_key="USER_INFO_"+user.getUserId();
        String user_info_json=stringRedisTemplate.opsForValue().get(user_info_key);
        ApUser apUser=null;
        if (StringUtils.isNotEmpty(user_info_json)){
            apUser=JSON.parseObject(user_info_json,ApUser.class);
        }else {
            ResponseResult<ApUser> result = userFeignClient.findById(user.getUserId());
            if (result.getCode()!=200||result.getData()==null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
            apUser=result.getData();
            stringRedisTemplate.opsForValue().set(user_info_key,JSON.toJSONString(apUser), Duration.ofHours(24));
        }
        //3. 封装评论数据
        ApComment apComment = new ApComment();
        apComment.setId(ObjectId.get().toHexString());
        apComment.setUserId(user.getUserId());
        apComment.setNickName(apUser.getNicknme());
        apComment.setImage(apUser.getImage());
        apComment.setContent(content);
        apComment.setTargetId(targetId);
        apComment.setLikes(0);
        apComment.setReply(0);
        apComment.setCreatedTime(new Date());
        apComment.setUpdatedTime(new Date());

        //4. 保存评论数据到mongodb
        mongoTemplate.save(apComment);

        return ResponseResult.okResult("评论成功");
    }

    @Override
    public ResponseResult like(String targetId, Short operation) {
        //判断用户是否登录
        UserInfo user = UserinfoThreadLocalUtil.getUser();
        if (user==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //判断用户是否点过赞
        String user_like_key="COMMENT_LIKE_"+user.getUserId();
        Boolean isMember = stringRedisTemplate.opsForSet().isMember(user_like_key, targetId);
        //之前未点赞--首次点赞到
        if (!isMember&&operation==0) {
            //新增点赞信息到mongdb
            ApCommentLike apCommentLike=new ApCommentLike();
            apCommentLike.setId(ObjectId.get().toHexString());
            apCommentLike.setTargetId(targetId);
            apCommentLike.setUserId(user.getUserId().intValue());
            apCommentLike.setCreatedTime(new Date());
            mongoTemplate.save(apCommentLike);

            Query query = Query.query(Criteria.where("_id").is(targetId));
            Update update=new Update();
            update.inc("likes",1);
            mongoTemplate.updateFirst(query,update,ApComment.class);
            //新增点赞数到redis
            stringRedisTemplate.opsForSet().add(user_like_key,targetId);
        }else if(isMember&&operation==1){
            //从mongodb删除点赞数据
            Query query = Query.query(Criteria.where("userId").is(user.getUserId()).and("targetId").is(targetId));
            mongoTemplate.remove(query,ApCommentLike.class);
            //从评论集合中点赞数量—1
            query=Query.query(Criteria.where("_id").is(targetId));
            Update update=new Update();
            update.inc("likes",-1);
            mongoTemplate.updateFirst(query,update,ApComment.class);
            stringRedisTemplate.opsForSet().remove(user_like_key,targetId);

        }

        ApComment comment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(targetId)), ApComment.class);


        Map<String, Object> resultMap = new HashMap<>();
        if (comment!=null) {
            resultMap.put("likes", comment.getLikes());
        }


        return ResponseResult.okResult(resultMap);
    }

    @Override
    public ResponseResult<List<ApCommentVo>> load(String targetId, Date minTime, Integer size) {
        //根据目标id查询目标下的评论列表
        //封装查询条件

        Criteria criteria = Criteria.where("targetId").is(targetId)
                .and(("createdTime")).lt(minTime);
        //执行查询操作
        Query query=Query.query(criteria).with(Sort.by("createdTime").descending()).limit(size);
        List<ApComment> apCommentList = mongoTemplate.find(query, ApComment.class);
        //2.判断查询出来的评论列表中，用户点赞的有哪些（加上点赞标识）
        //2.1如果用户没有登录 直接返回评论列表
        UserInfo user = UserinfoThreadLocalUtil.getUser();
        if (user==null){
            return ResponseResult.okResult(apCommentList);
        }
        //获取用户的点赞集合列表
        String user_like_key="COMMENT_LIKE_"+user.getUserId();
        Set<String> members = stringRedisTemplate.opsForSet().members(user_like_key);

        //2.2如果用户登录啦需要判断用户对那些评论点赞 加上点赞标识
        List<ApCommentVo> commentVoList = apCommentList.stream().map(apComment -> {
            ApCommentVo apCommentVo = new ApCommentVo();
            BeanUtil.copyProperties(apComment, apCommentVo);
            //判断是否点过赞
            boolean isLike = members.contains(apComment.getId());
            //根据判断结果去给实体类加上相应的点赞标识
            if (isLike) {
                apCommentVo.setOperation((short) 0);
            } else {
                apCommentVo.setOperation((short) 1);
            }
            return apCommentVo;
        }).collect(Collectors.toList());
        return ResponseResult.okResult(commentVoList);

    }
    @Override
    public ResponseResult saveCommentRepay(String targetId, String content) {
        //完成评论
        ResponseResult result = saveComment(targetId, content);
        if (result.getCode() == 200) {
            //评论回复完成, 评论的回复数量 + 1
            Query query = Query.query(Criteria.where("_id").is(targetId));
            Update update = new Update().inc("reply", 1);
            mongoTemplate.updateFirst(query, update, ApComment.class);
        }

        return result;
    }
}
