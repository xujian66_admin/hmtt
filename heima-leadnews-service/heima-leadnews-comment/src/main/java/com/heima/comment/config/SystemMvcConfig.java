package com.heima.comment.config;

import com.heima.comment.interceptor.UserInfoInterceptor;
import com.heima.common.jackson.JacksonConfig;
import com.heima.common.swagger.SwaggerConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
@Import({JacksonConfig.class, SwaggerConfiguration.class})
public class SystemMvcConfig implements WebMvcConfigurer {

    @Resource
    private UserInfoInterceptor userInfoInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userInfoInterceptor).addPathPatterns("/api/**");
    }
}