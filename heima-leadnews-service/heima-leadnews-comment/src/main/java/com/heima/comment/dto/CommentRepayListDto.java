package com.heima.comment.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CommentRepayListDto {

   private String commentId;

   private Date minDate;

   private Integer size;
}