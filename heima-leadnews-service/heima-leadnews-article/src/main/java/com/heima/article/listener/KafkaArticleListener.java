package com.heima.article.listener;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.model.common.constants.KafkaMessageConstants;
import com.heima.model.wemedia.entity.ApArticle;
import com.heima.model.wemedia.entity.ApArticleConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
@Slf4j
public class KafkaArticleListener {

    @Resource
    private ApArticleConfigMapper apArticleConfigMapper;

    @Resource
    private ApArticleMapper apArticleMapper;

    @KafkaListener(topics = KafkaMessageConstants.WM_NEWS_UP_DOWN_TOPIC, groupId = "WM_NEWS")
    public void upOrDown(ConsumerRecord<String, String> record) {
        String value = record.value();
        log.info("接收到消息:{}", value);
        if (StringUtils.isEmpty(value)) {
            return;
        }


        Map<String, String> map = JSONObject.parseObject(value, Map.class);
        Long newsId = Long.valueOf(map.get("newsId"));
        Short enable = Short.valueOf(map.get("enable"));
        //更新文章配置信息
        ApArticle article = apArticleMapper.selectOne(Wrappers.<ApArticle>lambdaQuery().eq(ApArticle::getNewsId, newsId));
        if (article == null) {
            return;
        }

        ApArticleConfig articleConfig = apArticleConfigMapper.selectOne(Wrappers.<ApArticleConfig>lambdaQuery().eq(ApArticleConfig::getArticleId, article.getId()));
        if (articleConfig == null) {
            return;
        }
        
        articleConfig.setEnable(enable.intValue());

        apArticleConfigMapper.updateById(articleConfig);
    }
}