package com.heima.article.service;

public interface ApArticleFreemarkerService {

    /**
     * 发布文章内容
     *
     * @param id 文章ID
     * @param content   文章内容数据
     */
    String buildArticle2Minio(Long id, String content);
}