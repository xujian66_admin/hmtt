package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleFreemarkerService;
import com.heima.article.service.ApArticleService;
import com.heima.model.common.constants.ArticleContants;
import com.heima.model.common.constants.KafkaMessageConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.vos.SearchArticleVo;
import com.heima.model.wemedia.dto.ArticleDto;
import com.heima.model.wemedia.dto.ArticleHomeDto;
import com.heima.model.wemedia.entity.ApArticle;
import com.heima.model.wemedia.entity.ApArticleConfig;
import com.heima.model.wemedia.entity.ApArticleContent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xujian
 * @date 2023/6/15 20:26
 */
@Service
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {
    @Resource
    private ApArticleMapper apArticleMapper;
    @Resource
    private ApArticleConfigMapper apArticleConfigMapper;
    @Resource
    private ApArticleContentMapper apArticleContentMapper;
    @Resource
    private KafkaTemplate kafkaTemplate;
    @Resource
    private ApArticleFreemarkerService apArticleFreemarkerService;

    @Override
    public ResponseResult<String> saveArticle(ArticleDto dto) {
        //校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //判断是新增操作还是修改操作 newsId
        ApArticle apArticle = apArticleMapper.selectOne(Wrappers.<ApArticle>lambdaQuery().eq(ApArticle::getNewsId, dto.getNewsId()));
        //如果是新增保存到数据库
        if (apArticle == null) {
            apArticle = new ApArticle();
            BeanUtils.copyProperties(dto, apArticle);
            apArticle.setLikes(0);
            apArticle.setCollection(0);
            apArticle.setViews(0);
            apArticle.setComment(0);
            apArticleMapper.insert(apArticle);

            //保存文章基本信息到数据库 ap_article_config
            ApArticleConfig apArticleConfig = new ApArticleConfig();
            apArticleConfig.setArticleId(apArticle.getId());
            apArticleConfig.setEnable(1);
            apArticleConfig.setIsDelete(0);
            apArticleConfig.setIsComment(1);
            apArticleConfig.setIsForward(1);
            apArticleConfigMapper.insert(apArticleConfig);
            //保存文章内容信息到数据库 ap_article_content
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setArticleId(apArticle.getId());
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.insert(apArticleContent);
        } else {
            //如果是修改更新数据到数据库
            // 修改文章基本信息 ap_article
            Long articleId = apArticle.getId();
            BeanUtils.copyProperties(dto, apArticle);
            apArticle.setId(articleId);
            apArticleMapper.updateById(apArticle);

            // 修改文章内容信息 p_article_content
            ApArticleContent apArticleContent = apArticleContentMapper.selectById(articleId);
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.updateById(apArticleContent);
        }
        String url = apArticleFreemarkerService.buildArticle2Minio(apArticle.getId(), dto.getContent());
        //4. 修改文章表, 回填静态URL地址
        apArticle.setStaticUrl(url);
        apArticleMapper.updateById(apArticle);
        //发送消息给MQ---->同步文章数给ES
        if (StringUtils.isEmpty(url)){
            throw new RuntimeException("生成文章静态详情页失败");
        }

        SearchArticleVo searchArticleVo = new SearchArticleVo();
        BeanUtils.copyProperties(apArticle, searchArticleVo);
        searchArticleVo.setContent(dto.getContent());
        searchArticleVo.setStaticUrl(url);

        kafkaTemplate.send(KafkaMessageConstants.AP_ARTICLE_ES_SYNC_TOPIC, JSON.toJSONString(searchArticleVo));

        return ResponseResult.okResult(apArticle.getId());


    }

    @Override
    public ResponseResult load(ArticleHomeDto dto, short loadTypeNew) {
        //1. 校验参数 ---为空设置默认值, 超过50设置为50
        Integer size = dto.getSize() == null || dto.getSize() <= 0 ? 10 : dto.getSize();
        size = Math.min(size, 50);

        // 2. 频道校验
        String tag = dto.getTag();
        tag = StringUtils.isEmpty(tag) ? ArticleContants.DEFAULT_TAG : tag;

        //3. 最小时间校验
        Date minTime = dto.getMinBehotTime();
        minTime = minTime == null ? new Date() : minTime;

        //3. 最大时间校验
        Date maxTime = dto.getMaxBehotTime();
        maxTime = maxTime == null ? new Date() : maxTime;

        //4. 封装查询参数
        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put("size", size);
        queryParam.put("tag", tag);
        queryParam.put("minTime", minTime);
        queryParam.put("maxTime", maxTime);
        queryParam.put("loadType", loadTypeNew);

        List<ApArticle> apArticleList = apArticleMapper.queryArticleListByCondition(queryParam);

        return ResponseResult.okResult(apArticleList);
    }
}
