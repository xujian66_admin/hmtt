package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.ApArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author xujian
 * @date 2023/6/15 20:27
 */
@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {
    List<ApArticle> queryArticleListByCondition(Map<String, Object> queryParam);
}
