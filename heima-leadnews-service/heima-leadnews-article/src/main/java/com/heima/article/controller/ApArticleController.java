package com.heima.article.controller;

import com.heima.article.service.ApArticleService;
import com.heima.model.common.constants.ArticleContants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.ArticleDto;
import com.heima.model.wemedia.dto.ArticleHomeDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/15 20:21
 */
@RestController
@RequestMapping(path = "/api/v1/article")
public class ApArticleController {
    @Resource
    private ApArticleService apArticleService;

    @PostMapping(path = "/save")
    public ResponseResult<String> saveArticle(@RequestBody ArticleDto dto) {
        return apArticleService.saveArticle(dto);
    }
    /**
     * APP首页默认加载文章列表   0 : 加载 更多  1 : 加载最新
     *
     * @param dto
     * @return
     */
    @PostMapping(path = "/load")
    public ResponseResult load(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleContants.LOAD_TYPE_MORE);
    }

    /**
     * APP首页默认加载更多文章数据
     *
     * @param dto
     * @return
     */
    @PostMapping(path = "/loadmore")
    public ResponseResult loadmore(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleContants.LOAD_TYPE_MORE);
    }

    /**
     * APP首页默认加载最新文章数据
     *
     * @param dto
     * @return
     */
    @PostMapping(path = "/loadnew")
    public ResponseResult loadnew(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleContants.LOAD_TYPE_NEW);
    }
}
