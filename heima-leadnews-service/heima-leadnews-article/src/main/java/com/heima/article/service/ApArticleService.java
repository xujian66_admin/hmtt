package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.ArticleDto;
import com.heima.model.wemedia.dto.ArticleHomeDto;
import com.heima.model.wemedia.entity.ApArticle;

/**
 * @author xujian
 * @date 2023/6/15 20:25
 */
public interface ApArticleService extends IService<ApArticle> {


    /**
     * APP文章发布
     *
     * @param dto
     * @return
     */
    ResponseResult<String> saveArticle(ArticleDto dto);

    ResponseResult load(ArticleHomeDto dto, short loadTypeNew);
}