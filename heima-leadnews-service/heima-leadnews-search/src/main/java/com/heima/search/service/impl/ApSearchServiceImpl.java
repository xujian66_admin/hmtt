package com.heima.search.service.impl;

import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.UserSearchDto;
import com.heima.model.user.pojo.ApUser;
import com.heima.search.pojo.ApUserSearch;
import com.heima.search.service.ApSearchService;
import com.heima.search.service.ApUserSearchService;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author Administrator
 * @Date 2023/6/23
 **/
@Service
public class ApSearchServiceImpl implements ApSearchService {

    @Resource
    private RestHighLevelClient restHighLevelClient;

    @Resource
    private ApUserSearchService apUserSearchService;


    @Override
    public ResponseResult search(UserSearchDto dto) throws IOException {
        //1. 校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //最小时间校验
        Date minTime = dto.getMinBehotTime();
        minTime = minTime == null ? new Date() : minTime;
        //每页展示数据条数校验
        int size = dto.getPageSize();
        size = size <= 0 ? 10 : size;
        size = Math.min(size, 50);

        ApUser apUser = AppUserThreadLocalUtil.get();

        if (apUser!=null&& dto.getFromIndex()==0){
            apUserSearchService.insert(dto.getSearchWords(),apUser.getId());
        }


        //2. 创建ES搜索请求对象
        SearchRequest request = new SearchRequest("app_article_info");
        //3. 设置DSL参数
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        //3.1 搜索关键词参数
        if (StringUtils.isNotEmpty(dto.getSearchWords())) {
            boolQuery.must(QueryBuilders.matchQuery("all", dto.getSearchWords()));
        } else {
            boolQuery.must(QueryBuilders.matchAllQuery());
        }

        //3.2 发布时间参数(根据上一页的最后一个文章的发布时间查询下一页数据)
        boolQuery.filter(QueryBuilders.rangeQuery("publishTime").lt(minTime));
        //设置查询条件
        request.source().query(boolQuery);
        //3.3 分页参数
        request.source().from(0).size(size);
        //3.4 排序参数
        request.source().sort("publishTime", SortOrder.DESC);
        //3.5 高亮参数
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title");
        highlightBuilder.preTags("<font style='color:red; font-size: inherit;'>");
        highlightBuilder.postTags("</font>");
        highlightBuilder.requireFieldMatch(false);

        request.source().highlighter(highlightBuilder);

        //4. 发送请求, 获取响应
        SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);
        //5. 解析响应结果
        //5.1 获取命中数据
        SearchHits hits = response.getHits();
        //5.2 获取命中数据列表
        SearchHit[] searchHits = hits.getHits();
        //5.3 遍历循环获取每一条命中数据
        List<Map<String, Object>> mapList = Arrays.stream(searchHits).map(searchHit -> {
            //获取命中数据的原始文档内容
            Map<String, Object> source = searchHit.getSourceAsMap();
            source.put("h_title", source.get("title"));
            //获取文档的高亮结果集
            Map<String, HighlightField> highlightFields = searchHit.getHighlightFields();
            if (!CollectionUtils.isEmpty(highlightFields)) {
                //获取指定字段名称的高亮结果
                HighlightField titleField = highlightFields.get("title");
                //判断对应字段的高亮结果集是否为空
                if (titleField != null) {
                    //获取高亮文本片段
                    Text[] texts = titleField.fragments();
                    String highlightResult = StringUtils.join(texts);
                    //封装高亮文本片段到原始文档, 返回
                    source.put("h_title", highlightResult);
                }
            }
            return source;
        }).collect(Collectors.toList());

        return ResponseResult.okResult(mapList);
    }


}