package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.UserSearchDto;

import java.io.IOException;

public interface ApSearchService {
    /**
     * 根据关键词搜索文章列表
     * @param dto
     * @return
     */
    ResponseResult search(UserSearchDto dto) throws IOException;


}
