package com.heima.search.service.impl;

import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dto.HistorySearchDto;
import com.heima.model.user.pojo.ApUser;
import com.heima.search.pojo.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author xujian
 * @date 2023/6/24 20:13
 */
@Service
public class ApUserSearchServiceImpl implements ApUserSearchService {
    @Resource
    private MongoTemplate mongoTemplate;
    @Override
    public void insert(String keyword, Long userId) {
        //校验参数

        //查询当前用户得到搜索关键词
        Query query = Query.query(Criteria.where("userId").is(userId).and("keyword").is(keyword));
        ApUserSearch ap = mongoTemplate.findOne(query, ApUserSearch.class);
        //存在 更新创建时间
        if (ap!=null){
            ap.setCreatedTime(new Date());
            mongoTemplate.save(ap);
            return;
        }
        //不存在 判断当前历史记录总数量是超过10

        query = Query.query(Criteria.where("userId").is(userId)).with(Sort.by("createdTime").ascending());
        List<ApUserSearch> apUserSearches = mongoTemplate.find(query, ApUserSearch.class);
        if (apUserSearches.size()>10){
            ApUserSearch apUserSearch = apUserSearches.get(0);
            mongoTemplate.remove(apUserSearch);
        }
        //重新赋值
        ap = new ApUserSearch();
        ap.setId(ObjectId.get().toHexString());
        ap.setUserId(userId.intValue());
        ap.setKeyword(keyword);
        ap.setCreatedTime(new Date());
        mongoTemplate.save(ap);

    }
    /**
     * 查询搜索历史
     *
     * @return
     */
    @Override
    public ResponseResult findUserSearch() {
        //获取当前用户
        ApUser user = AppUserThreadLocalUtil.get();
        if(user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //根据用户查询数据，按照时间倒序
        List<ApUserSearch> apUserSearches = mongoTemplate.find(Query.query(Criteria.where("userId").is(user.getId())).with(Sort.by(Sort.Direction.DESC, "createdTime")), ApUserSearch.class);
        return ResponseResult.okResult(apUserSearches);
    }

    @Override
    public ResponseResult delete(HistorySearchDto historySearchDto) {
        //1.检查参数
        if(historySearchDto.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.判断是否登录
        ApUser user = AppUserThreadLocalUtil.get();
        if(user == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //3.删除
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(user.getId()).and("id").is(historySearchDto.getId())),ApUserSearch.class);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
