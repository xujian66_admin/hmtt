package com.heima.search.config;

import com.heima.search.interceptor.AppTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/24 19:36
 */
@Configuration
public class WmMvcConfig implements WebMvcConfigurer {
    @Resource
    private AppTokenInterceptor appTokenInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(appTokenInterceptor).addPathPatterns("/api/**");
    }
}
