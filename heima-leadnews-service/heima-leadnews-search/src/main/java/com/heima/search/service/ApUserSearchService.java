package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dto.HistorySearchDto;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


public interface ApUserSearchService {

    /**
     * 保存用户搜索历史记录
     * @param keyword
     * @param userId
     */
    @Async("taskExecutor")
    public void insert(String keyword,Long userId);
    /**
     查询搜索历史
     @return
     */
    ResponseResult findUserSearch();
    /**
     * 删除搜索历史
     *
     * @param historySearchDto
     * @return
     */
    ResponseResult delete(HistorySearchDto historySearchDto);
}