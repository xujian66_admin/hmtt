package com.heima.search.interceptor;

import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.model.user.pojo.ApUser;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author xujian
 * @date 2023/6/24 18:16
 */
@Component
public class AppTokenInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取请求头
        String userId = request.getHeader("userId");
        if (StringUtils.isNotBlank(userId)&&!StringUtils.equals("0",userId)) {
            ApUser apUser = new ApUser();
            apUser.setId(Long.valueOf(userId));
            AppUserThreadLocalUtil.set(apUser);
        }
        return super.preHandle(request,response,handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        AppUserThreadLocalUtil.clean();
        super.postHandle(request, response, handler, modelAndView);
    }
}
