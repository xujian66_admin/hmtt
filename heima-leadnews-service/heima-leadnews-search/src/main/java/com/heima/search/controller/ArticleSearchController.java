package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.UserSearchDto;
import com.heima.search.service.ApSearchService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/article/search")
public class ArticleSearchController {

    @Resource
    private ApSearchService apSearchService;

    @PostMapping("/search")
    public ResponseResult search(@RequestBody UserSearchDto dto) throws IOException {
        return apSearchService.search(dto);
    }
}