package com.heima.recommend.task;

import com.heima.recommend.service.HotArticleService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/29 21:23
 */
@Component
@Slf4j
public class HotArticleComputeTask {

    @Resource
    private HotArticleService hotArticleService;

    //@Scheduled(cron = "0/30 * * * * ?")
    @XxlJob("computeHotArticleJob")
    public void hotArticleCompute(){

        log.info("热点文章计算任务开始执行——————————");
        hotArticleService.computeHotArticle();
        log.info("热点文章计算任务执行结束——————————");
    }
    @XxlJob("clearRecommentData")
    public void clearHotArticle(){

        log.info("清理热点文章计算任务开始执行——————————");
        hotArticleService.clearComputeHot();
        log.info("清理热点文章计算任务执行结束——————————");
    }
}
