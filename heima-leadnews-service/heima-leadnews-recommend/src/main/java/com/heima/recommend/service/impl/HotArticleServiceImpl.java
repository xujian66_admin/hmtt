package com.heima.recommend.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.heima.model.common.constants.ArticleContants;
import com.heima.model.recommend.HotArticleVo;
import com.heima.model.wemedia.entity.ApArticle;
import com.heima.recommend.mapper.ApArticleMapper;
import com.heima.recommend.service.HotArticleService;
import com.heima.recommend.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xujian
 * @date 2023/6/29 16:35
 */
@Service
@Slf4j
public class HotArticleServiceImpl implements HotArticleService {
    @Resource
    private ApArticleMapper apArticleMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private RedisUtils redisUtils;
    @Override
    public void computeHotArticle() {
        Date begin = DateUtil.offsetDay(new Date(), -10).toJdkDate();
        Integer size=5;
        while (true){
            List<ApArticle> apArticleList= apArticleMapper.queryArticleByLimit(begin,size);
            if (CollectionUtils.isEmpty(apArticleList)){
                break;
            }
            begin=apArticleList.get(apArticleList.size()-1).getPublishTime();

            log.info("开始热点文章的计算和缓存,线程名称：{}",Thread.currentThread().getName());
            threadPoolTaskExecutor.execute(()->computeAndCacheHotArticle(apArticleList));

        }

    }

    @Override
    public void clearComputeHot() {

        Set<String> keys = redisUtils.scan(ArticleContants.HOT_ARTICLE_PREFIX + "*");
        log.info("扫描到所有需要清楚的key：{}",keys);
        stringRedisTemplate.delete(keys);


    }

    private void computeAndCacheHotArticle(List<ApArticle> apArticleList) {
        //2.根据权重计算文章的推荐分值
        List<HotArticleVo> articleVoList=computeHotArticleScore(apArticleList);
        //3.为推荐频道和每个频道推荐数据

        saveHotArticleToCache(articleVoList);
    }

    /**
     * @Async("taskExecutor)失效 @Async注解失效的场景比如方法为私有
     * 方法被private修饰
     * 没有使用代理对象调用
     *
     * 解决方案
     * 1.手动提交任务到线程池
     * 2.使用代理对象调用
     *
     * @param articleVoList
     */
    private void saveHotArticleToCache(List<HotArticleVo> articleVoList) {
        if (CollectionUtils.isEmpty(articleVoList)) {
            return;
        }
        //为推荐频道缓存文章数据ZSET
        //Map<Long,List<HotArticleVo>> channels=new HashMap<>();
        //for (HotArticleVo hotArticleVo : articleVoList) {
        //    List<HotArticleVo> voList = channels.get(hotArticleVo.getChannelId());
        //    if (CollectionUtils.isEmpty(voList)) {
        //        voList=new ArrayList<>();
        //        voList.add(hotArticleVo);
        //        channels.put(hotArticleVo.getChannelId(),voList);
        //    }else {
        //        voList.add(hotArticleVo);
        //    }
        //}
        Map<Long,List<HotArticleVo>> channels=articleVoList.stream().collect(Collectors.groupingBy(HotArticleVo::getChannelId));


        //2.为每一个频道缓存文章数据ZSET
        channels.forEach((channelId,hotArticleVoList)->{
            String hot_article_channel_key=ArticleContants.HOT_ARTICLE_PREFIX+"_"+channelId;
            String hot_article_all_key=ArticleContants.HOT_ARTICLE_PREFIX+ArticleContants.DEFAULT_TAG;
            String article_info_key=ArticleContants.HOT_ARTICLE_INFO_KEY;
            /**
             * 频繁操作Redis优化
             * 1.使用Redis管道操作进行优化pipeline
             * 2.使用Redis批处理指令进行优化
             *
             * 如果一批次命令中涉及到多种数据类型的多个不同操作 可以使用pipeline
             * 如果一批次命令操作数据类型一样可以使用批处理指令
             */
            //hotArticleVoList.stream().forEach(hotArticleVo -> {
            //    //为每一个频道缓存文章数据
            //    stringRedisTemplate.opsForZSet().add(hot_article_channel_key,hotArticleVo.getId().toString(),hotArticleVo.getScore());
            //    //为推荐频道缓存文章数据ZSET
            //    stringRedisTemplate.opsForZSet().add(hot_article_all_key,hotArticleVo.getId().toString(), hotArticleVo.getScore());
            //    //缓存文章基本信息到Redis Hash
            //    stringRedisTemplate.opsForHash().put(article_info_key,hotArticleVo.getId().toString(), JSON.toJSONString(hotArticleVo));
            //});

            //为每一个频道缓存文章数据ZSET----> 把频道下的每一个文章数据转化为TypeTuple
            Set<ZSetOperations.TypedTuple<String>> typedTupleSet = hotArticleVoList.stream()
                    .map(hotArticleVo -> new DefaultTypedTuple<String>(hotArticleVo.getId().toString(), hotArticleVo.getScore().doubleValue()))
                    .collect(Collectors.toSet());
            stringRedisTemplate.opsForZSet().add(hot_article_channel_key,typedTupleSet);
            stringRedisTemplate.opsForZSet().add(hot_article_all_key,typedTupleSet);

            Map<String, String> articleInfoMap = hotArticleVoList.stream().collect(Collectors.toMap(hotArticleVo -> hotArticleVo.toString(), hotArticleVo -> JSON.toJSONString(hotArticleVo)));

            stringRedisTemplate.opsForHash().putAll(article_info_key,articleInfoMap);
        });


        //3.缓存文章基本信息到Redis Hash
    }

    /**
     * 计算每一个文章的分值数据
     * @param apArticleList
     * @return
     */
    private List<HotArticleVo> computeHotArticleScore(List<ApArticle> apArticleList) {
        if (CollectionUtils.isEmpty(apArticleList)) {
            return Collections.emptyList();
        }
        //封装分值参数

        List<HotArticleVo> hotArticleVoList = apArticleList.stream().map(apArticle -> {
            HotArticleVo vo = new HotArticleVo();
            BeanUtil.copyProperties(apArticle, vo);
            Integer score = computeScore(apArticle);
            vo.setScore(score);
            return vo;

        }).collect(Collectors.toList());
        return hotArticleVoList;
    }

    private Integer computeScore(ApArticle apArticle) {
       int score=0;
       if (apArticle.getLikes()!=null){
           score+=apArticle.getLikes()* ArticleContants.HOT_ARTICLE_LIKE_WEIGHT;
       }
       if (apArticle.getCollection()!=null){
           score+=apArticle.getCollection()*ArticleContants.HOT_ARTICLE_COLLECT_WEIGHT;
       }
        if (apArticle.getComment()!=null) {
            score+=apArticle.getComment()*ArticleContants.HOT_ARTICLE_COMMENT_WEIGHT;
        }
        if (apArticle.getViews()!=null){
            score+= apArticle.getViews()*ArticleContants.HOT_ARTICLE_VIEW_WEIGHT;
        }
        return score;
    }
}
