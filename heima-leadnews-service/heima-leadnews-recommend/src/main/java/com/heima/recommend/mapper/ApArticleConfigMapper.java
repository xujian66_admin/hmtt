package com.heima.recommend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.ApArticleConfig;

/**
 * @author xujian
 * @date 2023/6/15 21:15
 */
public interface ApArticleConfigMapper extends BaseMapper<ApArticleConfig> {
}
