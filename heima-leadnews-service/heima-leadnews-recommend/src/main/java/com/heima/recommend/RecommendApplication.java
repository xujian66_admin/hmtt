package com.heima.recommend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author xujian
 * @date 2023/6/29 9:37
 */
@SpringBootApplication
@EnableScheduling
@MapperScan(basePackages = "com.heima.recommend.mapper")
public class RecommendApplication {
    public static void main(String[] args) {
        SpringApplication.run(RecommendApplication.class,args);
    }
}
