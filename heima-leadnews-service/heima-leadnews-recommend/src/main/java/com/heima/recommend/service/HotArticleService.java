package com.heima.recommend.service;

/**
 * @author xujian
 * @date 2023/6/29 16:33
 */
public interface HotArticleService {
    /**
     * 计算热文章数据
     */
    public void computeHotArticle();

    void clearComputeHot();
}
