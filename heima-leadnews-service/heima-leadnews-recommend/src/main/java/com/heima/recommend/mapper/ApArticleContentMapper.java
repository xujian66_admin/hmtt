package com.heima.recommend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.ApArticleContent;

/**
 * @author xujian
 * @date 2023/6/15 21:16
 */
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}
