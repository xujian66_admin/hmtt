package com.heima.behavior.dtos;

import lombok.Data;

@Data
public class UnLikesBehaviorDto {

    /**
     * 文章、动态、评论等ID
     */
    private Long articleId;

    /**
     * 不喜欢操作方式
     * 0 不喜欢
     * 1 取消不喜欢
     */
    private short type;

}