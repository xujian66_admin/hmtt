package com.heima.behavior.pojos;

import cn.hutool.core.annotation.Alias;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * APP不喜欢行为表
 * </p>
 *
 * @author itheima
 */
@Data
@Document("ap_unlikes_behavior")
public class ApUnlikesBehavior implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    /**
     * 实体ID
     */
    private Long userId;

    /**
     * 文章ID
     */
    private Long articleId;

    /**
     * 0 不喜欢
     * 1 取消不喜欢
     */
    private Short type;

    /**
     * 登录时间
     */
    private Date createdTime;

}