package com.heima.behavior.interceptor;

import com.alibaba.fastjson.JSON;
import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserinfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojo.ApUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author Administrator
 * @Date 2023/6/12
 **/
@Component
public class UserInfoInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1. 获取请求中的userId头信息
        String userId = request.getHeader("userId");
        if (StringUtils.isNotEmpty(userId) && !StringUtils.equals(userId, "0")) {
            //2. 设置userId到ThreadLocal中
            UserInfo userInfo = new UserInfo();
            userInfo.setUserId(Long.valueOf(userId));

            ApUser apUser=new ApUser();
            apUser.setId(Long.valueOf(userId));
            AppUserThreadLocalUtil.set(apUser);
            UserinfoThreadLocalUtil.setUser(userInfo);

            return super.preHandle(request, response, handler);
        }

        //没有userId代表没有认证, 返回需要认证
        ResponseResult result = ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(result));
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        UserinfoThreadLocalUtil.clear();
        AppUserThreadLocalUtil.clean();
        super.postHandle(request, response, handler, modelAndView);
    }
}