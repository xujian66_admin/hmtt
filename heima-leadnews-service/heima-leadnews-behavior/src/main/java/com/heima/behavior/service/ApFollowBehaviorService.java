package com.heima.behavior.service;

import com.heima.behavior.dtos.FollowBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApFollowBehaviorService {
    /**
     * 关注用户
     *
     * @param dto
     * @return
     */
    ResponseResult follow(FollowBehaviorDto dto);
}