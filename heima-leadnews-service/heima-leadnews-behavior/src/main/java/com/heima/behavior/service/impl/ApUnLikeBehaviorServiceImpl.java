package com.heima.behavior.service.impl;

import com.heima.behavior.dtos.UnLikesBehaviorDto;
import com.heima.behavior.pojos.ApUnlikesBehavior;
import com.heima.behavior.service.ApUnLikeBehaviorService;
import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojo.ApUser;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ApUnLikeBehaviorServiceImpl implements ApUnLikeBehaviorService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private MongoTemplate mongoTemplate;
    @Override
    public ResponseResult unlike(UnLikesBehaviorDto dto) {
        //校验参数
        if (dto.getArticleId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //感知用户状态
        ApUser apUser = AppUserThreadLocalUtil.get();
        if (apUser==null){
            return ResponseResult.okResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        String unlike_hash_key="USER_UNLIKE_"+dto.getArticleId();
        Boolean hasKey = stringRedisTemplate.opsForHash().hasKey(unlike_hash_key, apUser.getId() + "");
        //用户点过不喜欢--重复点击
        if (hasKey&&dto.getType()==0) {
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
        //用户点击过不喜欢--点击到取消不喜欢
        if (hasKey&&dto.getType()==1){
            //删除缓存中的数据
            stringRedisTemplate.opsForHash().delete(unlike_hash_key,apUser.getId());
            //删除mongodb中的数据
            Query query = Query.query(Criteria.where("userId").is(apUser.getId()).and("articleId").is(dto.getArticleId()));
            mongoTemplate.remove(query, ApUnlikesBehavior.class);
        }
        //用户没有点击过不喜欢--点击到不喜欢
        if (!hasKey&&dto.getType()==0){
            stringRedisTemplate.opsForHash().put(unlike_hash_key,apUser.getId(),"1");
            ApUnlikesBehavior apUnlikesBehavior=new ApUnlikesBehavior();
            apUnlikesBehavior.setArticleId(dto.getArticleId());
            apUnlikesBehavior.setId(apUser.getId().toString());
            apUnlikesBehavior.setCreatedTime(new Date());

        }
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

    }
}
