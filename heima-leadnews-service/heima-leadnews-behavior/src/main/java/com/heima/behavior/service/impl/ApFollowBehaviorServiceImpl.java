package com.heima.behavior.service.impl;


import com.heima.behavior.dtos.FollowBehaviorDto;
import com.heima.behavior.pojos.ApFollowBehavior;
import com.heima.behavior.service.ApFollowBehaviorService;
import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojo.ApUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;


@Slf4j
@Service
public class ApFollowBehaviorServiceImpl implements ApFollowBehaviorService {

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public ResponseResult follow(FollowBehaviorDto dto) {

        //1. 校验参数
        if (dto.getArticleId() == null || dto.getAuthorId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        ApUser apUser = AppUserThreadLocalUtil.get();
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //2. 判断用户是否已经互相关注
        String follow_hash_key = "USER_FOLLOW_" + apUser.getId();
        String fans_hash_key = "USER_FANS_" + dto.getAuthorId();
        
        Boolean hasKey = stringRedisTemplate.opsForHash().hasKey(follow_hash_key, dto.getAuthorId()+"");
        //2.1 用户已关注 --- 关注
        if (hasKey && dto.getOperation() == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SUCCESS);
        }
        
        //2.2 用户已关注 --- 取消关注
        if (hasKey && dto.getOperation() == 1) {
            //2.2.1 删除缓存数据
            stringRedisTemplate.opsForHash().delete(follow_hash_key, dto.getAuthorId()+"");
            stringRedisTemplate.opsForHash().delete(fans_hash_key, apUser.getId()+"");
            //2.2.2 删除mongodb数据
            Query query = Query.query(Criteria.where("userId").is(apUser.getId()).and("followId").is(dto.getAuthorId()));
            mongoTemplate.remove(query, ApFollowBehavior.class);
            return ResponseResult.errorResult(AppHttpCodeEnum.SUCCESS);
        }

        //2.3 用户未关注
        if (!hasKey && dto.getOperation() == 0) {
            //2.3.1 保存关注数据到redis
            stringRedisTemplate.opsForHash().put(follow_hash_key, dto.getAuthorId()+"", "1");
            stringRedisTemplate.opsForHash().put(fans_hash_key, apUser.getId()+"", "1");
            
            //2.3.2 保存关注数据到数据库
            ApFollowBehavior apFollowBehavior = new ApFollowBehavior();
            apFollowBehavior.setArticleId(dto.getArticleId());
            apFollowBehavior.setCreatedTime(new Date());
            apFollowBehavior.setFollowId(dto.getAuthorId());
            apFollowBehavior.setUserId(apUser.getId());

            mongoTemplate.insert(apFollowBehavior);
        }


        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}