package com.heima.behavior.service;

import com.heima.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApUnLikeBehaviorService {
    ResponseResult unlike(UnLikesBehaviorDto dto);
}
