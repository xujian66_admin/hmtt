package com.heima.behavior.controller;

import com.heima.behavior.dtos.ArticleBehaviorDto;
import com.heima.behavior.service.ApBehaviorServcie;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/api/v1/article")
public class ApBehaviorController {

    @Resource
    private ApBehaviorServcie apBehaviorServcie;

    /**
     * 加载行为列表
     *
     * @return
     */
    @PostMapping(path = "/load_article_behavior")
    public ResponseResult loadArticleBehavior(@RequestBody ArticleBehaviorDto dto) {
        return apBehaviorServcie.loadArticleBehavior(dto);
    }
}