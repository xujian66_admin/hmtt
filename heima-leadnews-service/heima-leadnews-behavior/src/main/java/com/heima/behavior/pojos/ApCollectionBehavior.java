package com.heima.behavior.pojos;


import cn.hutool.core.annotation.Alias;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * APP收藏信息表
 * </p>
 *
 * @author itheima
 */
@Data
@Document("ap_collection")
public class ApCollectionBehavior implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 文章ID
     */
    private Long articleId;

    /**
     * 点赞内容类型
     * 0收藏
     * 1取消收藏
     */
    private Short type;

    /**
     * 创建时间
     */
    private Date collectionTime;
}