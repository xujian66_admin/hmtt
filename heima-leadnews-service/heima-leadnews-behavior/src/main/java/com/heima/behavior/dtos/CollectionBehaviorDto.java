package com.heima.behavior.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class CollectionBehaviorDto {

    /**
     * 文章ID
     */
    Long articleId;

    /**
     * 收藏内容类型
     * 0文章
     * 1动态
     */
    short type;

    /**
     * 操作类型
     * 0收藏
     * 1取消收藏
     */
    short operation;

    /**
     * 收藏时间
     */
    Date createTime;

}