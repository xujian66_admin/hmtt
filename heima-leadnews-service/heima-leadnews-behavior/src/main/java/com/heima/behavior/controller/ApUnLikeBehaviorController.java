package com.heima.behavior.controller;


import com.heima.behavior.dtos.UnLikesBehaviorDto;
import com.heima.behavior.service.ApUnLikeBehaviorService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/api/v1/unlike_behavior")
public class ApUnLikeBehaviorController {
    @Resource
    private ApUnLikeBehaviorService apUnLikeBehaviorService;

    public ResponseResult unlike(@RequestBody UnLikesBehaviorDto dto){
        return apUnLikeBehaviorService.unlike(dto);
    }
}
