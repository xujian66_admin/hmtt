package com.heima.behavior.service.impl;

import com.heima.behavior.dtos.CollectionBehaviorDto;
import com.heima.behavior.pojos.ApCollectionBehavior;
import com.heima.behavior.service.ApCollectionBehaviorService;
import com.heima.common.thread.AppUserThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojo.ApUser;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ApCollectionBehaviorServiceImpl implements ApCollectionBehaviorService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private MongoTemplate mongoTemplate;
    @Override
    public ResponseResult collection(CollectionBehaviorDto dto) {
        //校验参数
        if (dto.getArticleId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //判断用户状态
        ApUser apUser = AppUserThreadLocalUtil.get();
        if (apUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //用户已经收藏点击收藏按钮
        String collect_hash_key="USER_COLLECT_"+apUser.getId();
        Boolean hasKey = stringRedisTemplate.opsForHash().hasKey(collect_hash_key, apUser.getId());
        //
        if (hasKey&&dto.getOperation()==0){
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
        //用户收藏过--点击取消收藏
        if (hasKey&&dto.getOperation()==1){
            //从redis删除数据
            stringRedisTemplate.opsForHash().delete(collect_hash_key,apUser.getId());
            //从mongodb删除数据
            Query query = Query.query(Criteria.where("userId").is(apUser.getId()).and("articleId").is(dto.getArticleId()));
            mongoTemplate.remove(query, ApCollectionBehavior.class);
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

        }
        if (!hasKey&&dto.getOperation()==0){
            stringRedisTemplate.opsForHash().put(collect_hash_key,apUser.getId(),"1");
            ApCollectionBehavior apCollectionBehavior=new ApCollectionBehavior();
            apCollectionBehavior.setCollectionTime(new Date());
            apCollectionBehavior.setType((short) 1);
            apCollectionBehavior.setUserId(apUser.getId());
            apCollectionBehavior.setArticleId(dto.getArticleId());
            mongoTemplate.insert(apCollectionBehavior);
        }

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
