package com.heima.behavior.service;

import com.heima.behavior.dtos.ArticleBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApBehaviorServcie {
    /**
     * 查询文章行为数据
     *
     * @param dto
     * @return
     */
    ResponseResult loadArticleBehavior(ArticleBehaviorDto dto);
}