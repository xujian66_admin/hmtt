package com.heima.behavior.service;

import com.heima.behavior.dtos.ReadBehaviorDto;
import com.heima.behavior.pojos.ApReadBehavior;
import com.heima.model.common.dtos.ResponseResult;

public interface ApReadBehaviorService {

    /**
     * 保存阅读行为
     * 1. 阅读行为不需要回显和查询, 所以不需要保存到redis, 直接保存到数据库即可
     * 2. 阅读行为也不需要用户登录
     *
     * @param dto
     * @return
     */
    ResponseResult readBehavior(ReadBehaviorDto dto);
}