package com.heima.behavior.controller;

import com.heima.behavior.dtos.FollowBehaviorDto;
import com.heima.behavior.service.ApFollowBehaviorService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/v1/follow_behavior")
public class ApFollowBehaviorController {

    @Resource
    private ApFollowBehaviorService apFollowBehaviorService;

    @PostMapping(path = "/user_follow")
    public ResponseResult follow(@RequestBody FollowBehaviorDto dto) {
        return apFollowBehaviorService.follow(dto);
    }
}