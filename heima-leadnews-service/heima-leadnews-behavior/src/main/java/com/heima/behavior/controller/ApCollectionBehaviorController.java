package com.heima.behavior.controller;

import com.heima.behavior.dtos.CollectionBehaviorDto;
import com.heima.behavior.service.ApCollectionBehaviorService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/api/v1/collection_behavior")
public class ApCollectionBehaviorController {

    @Resource
    private ApCollectionBehaviorService apCollectionBehaviorService;

    @PostMapping
    public ResponseResult collection(@RequestBody CollectionBehaviorDto dto){
        return apCollectionBehaviorService.collection(dto);
    }
}
