package com.heima.behavior.service;

import com.heima.behavior.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApCollectionBehaviorService {
    ResponseResult collection(CollectionBehaviorDto dto);
}
