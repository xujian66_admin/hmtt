package com.heima.wemedia.scan.service;


import com.heima.model.wemedia.entity.WmNews;

import com.heima.wemedia.mapper.WmNewsMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class WmNewsAutoScanServiceTest {

    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;

    @Resource
    private WmNewsMapper wmNewsMapper;

    @Test
    void newsAutoScan() {
        WmNews wmNews = wmNewsMapper.selectById(4);
        wmNewsAutoScanService.newsAutoScan(wmNews);
    }
}