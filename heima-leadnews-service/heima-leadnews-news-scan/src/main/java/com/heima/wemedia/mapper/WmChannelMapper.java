package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmChannel;

/**
 * 自媒体频道表(WmChannel)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 09:21:38
 */
public interface WmChannelMapper extends BaseMapper<WmChannel> {

}

