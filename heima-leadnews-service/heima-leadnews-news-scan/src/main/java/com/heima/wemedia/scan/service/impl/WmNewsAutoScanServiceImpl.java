package com.heima.wemedia.scan.service.impl;

import com.alibaba.fastjson.JSON;
import com.hei.feign.client.ApArticleFeignClient;
import com.hei.feign.client.TaskFeignClient;
import com.heima.common.exception.CustomException;
import com.heima.file.MinIOTemplate;
import com.heima.green.BaiduTemplate;
import com.heima.green.pojo.ScanResult;
import com.heima.model.common.constants.TaskTypeEnum;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.wemedia.dto.ArticleDto;
import com.heima.model.wemedia.entity.WmChannel;
import com.heima.model.wemedia.entity.WmNews;
import com.heima.model.wemedia.entity.WmUser;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.utils.common.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmUserDao;
import com.heima.wemedia.scan.service.WmNewsAutoScanService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xujian
 * @date 2023/6/15 16:46
 */
@Service
public class WmNewsAutoScanServiceImpl implements WmNewsAutoScanService {


    @Resource
    private WmNewsMapper wmNewsMapper;
    @Resource
    private BaiduTemplate baiduTemplate;
    @Resource
    private MinIOTemplate minIOTemplate;
    @Resource
    private WmUserDao wmUserDao;
    @Resource
    private WmChannelMapper wmChannelMapper;
    @Resource
    private ApArticleFeignClient apArticleFeignClient;
    @Resource
    private TaskFeignClient taskFeignClient;

    @Override
    public void newsAutoScan(WmNews wmNews) {
        //判断文章状态是否是待审核
        if (wmNews.getStatus() != 1) {
            return;
        }
        //抽取文章中的文本和图片数据
        Map<String, Object> imageAndText = extractImageAndText(wmNews);
        //调用本地敏感词
        String text = imageAndText.get("text").toString();
        ScanResult sensitiveWordScan = sensitiveWordScan(text);
        if (sensitiveWordScan.getConclusionType() != 1) {
            updateWmNewsStatus(wmNews, sensitiveWordScan);
            return;
        }
        //调用百度Ai对文章中的文本进行审核
        ScanResult textScanResult = textScan(text);
        if (textScanResult.getConclusionType() != 1) {
            updateWmNewsStatus(wmNews, textScanResult);
            return;
        }
        //调用百度Ai对文章中的图片进行审核
        List<String> images = (List<String>) imageAndText.get("images");
        ScanResult imageScanResult = imageScan(images);
        if (imageScanResult.getConclusionType() != 1) {
            updateWmNewsStatus(wmNews, imageScanResult);
            return;
        }

        //更新文章中的审核状态
        updateWmNewsStatus(wmNews, new ScanResult("合格", 1, Collections.emptyList()));

       //if (wmNews.getPublishTime().getTime() <= System.currentTimeMillis()) {
       //    saveApArticle(wmNews);
       //    //修改文章状态为审核通过已发布
       //    wmNews.setStatus(9);
       //    wmNewsMapper.updateById(wmNews);
       //}

        wmNewsAutoPublish(wmNews.getId(), wmNews.getPublishTime());
    }

    private void wmNewsAutoPublish(Long id, Date publishTime) {
        TaskDto taskDto = new TaskDto();
        taskDto.setTaskType(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType());
        taskDto.setPriority(TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        taskDto.setExecuteTime(publishTime.getTime());

        //修改之后的实体信息只有id
        WmNews wmNews = new WmNews();
        wmNews.setId(id);
        taskDto.setParameters(ProtostuffUtil.serialize(wmNews));

        ResponseResult<Long> responseResult = taskFeignClient.addTask(taskDto);
        if (responseResult.getCode().intValue() != 200) {
            throw new RuntimeException("文章定时发布任务保存失败");
        }
    }

    private void saveApArticle(WmNews wmNews){
        ArticleDto articleDto=new ArticleDto();
        BeanUtils.copyProperties(wmNews,articleDto,"id");
        articleDto.setNewsId(wmNews.getId());
        articleDto.setAuthorId(wmNews.getUserId());
        WmUser wmUser=wmUserDao.selectById(wmNews.getUserId());
        if (wmUser!=null) {
            articleDto.setAuthorName(wmUser.getNickname());
        }
        //封装频道信息
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if(wmChannel!=null){
            articleDto.setChannelName(wmChannel.getName());
        }
        articleDto.setLayout(wmNews.getType());
        ResponseResult<String> result = apArticleFeignClient.save(articleDto);
        if (result.getCode() != 200) {
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }

    }
    /**
     * 判断审核结果更新文章状态
     *
     * @param wmNews
     * @param scanResult
     */
    private void updateWmNewsStatus(WmNews wmNews, ScanResult scanResult) {
        //判断审核结果更新文章状态
        Integer status = null;
        Integer conclusionType = scanResult.getConclusionType();
        if (conclusionType == 1) {
            status = 8;
        } else if (conclusionType == 2) {
            status = 2;
        } else if (conclusionType == 3) {
            status = 3;
        } else {
            status = 2;
        }
        wmNews.setStatus(status);
        wmNews.setReason(StringUtils.join(scanResult.getWords(), ","));
        wmNewsMapper.updateById(wmNews);


    }

    /**
     * 本地敏感词处理
     *
     * @param text
     * @return
     */
    private ScanResult sensitiveWordScan(String text) {
        //本地敏感词处理列表

        //3.判断文本中是否包含敏感词
        Map<String, Integer> result = SensitiveWordUtil.matchWords(text);
        if (CollectionUtils.isEmpty(result)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        //4.获取到敏感词列表，封装返回结果
        List<String> list = result.keySet().stream().collect(Collectors.toList());
        return new ScanResult("违规", 2, list);
    }

    /**
     * 百度云文本识别
     *
     * @param text
     * @return
     */
    private ScanResult textScan(String text) {
        if (StringUtils.isEmpty(text)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        return baiduTemplate.textScan(text);
    }

    /**
     * 百度问图片识别
     *
     * @param images
     * @return
     */
    private ScanResult imageScan(List<String> images) {
        if (CollectionUtils.isEmpty(images)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        List<ScanResult> resultList = images.stream().map(image -> minIOTemplate.downLoadFile(image))
                .map(bytes -> baiduTemplate.imageScan(bytes)).collect(Collectors.toList());
        //判断是否审核通过所有审核结果都是1，还是审核失败（有任何一个审核状态不是1）
        if (resultList.stream().allMatch(scanResult -> scanResult.getConclusionType() == 1)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        if (resultList.stream().anyMatch(scanResult -> scanResult.getConclusionType() == 2)) {
            return new ScanResult("不合格", 2, Arrays.asList("图片内容中包含不合格信息"));
        }
        if (resultList.stream().anyMatch(scanResult -> scanResult.getConclusionType() == 3)) {
            return new ScanResult("疑似", 3, Arrays.asList("图片内容中疑似包含不合格信息"));
        }
        return new ScanResult("审核失败", 4, Arrays.asList("审核失败"));
    }

    /**
     * 获取文本内的数据
     *
     * @param wmNews
     * @return
     */
    private Map<String, Object> extractImageAndText(WmNews wmNews) {
        //文本：标题，内容
        //图片：封面，内容
        //存储文本信息使用StringBuilder
        StringBuilder sb = new StringBuilder();
        //存储图片信息使用List集合
        List<String> urlList = new ArrayList<>();

        //提取文章标题
        sb.append(wmNews.getTitle());
        //抽取文章封面
        String images = wmNews.getImages();
        if (StringUtils.isNotEmpty(images)) {
            urlList.addAll(Arrays.asList(images.split(",")));
        }
        //抽取文章内容部分的文本和图片
        List<Map> mapList = JSON.parseArray(wmNews.getContent(), Map.class);

        mapList.stream().forEach(
                map -> {
                    //如果是文本拼接到字符串缓冲区
                    if ("text".equals(map.get("type"))) {
                        sb.append(map.get("value") + "@");
                    }
                    //如果是图片 , 添加到素材集合
                    if ("image".equals(map.get("type"))) {
                        urlList.add(map.get("value").toString());
                    }
                }
        );
        Map<String, Object> result = new HashMap<>();
        result.put("text", sb);
        result.put("images", urlList);
        return result;
    }


}
