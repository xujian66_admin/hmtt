package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmNews;

/**
 * @author xujian
 * @date 2023/6/15 16:43
 */
public interface WmNewsMapper extends BaseMapper<WmNews> {
}
