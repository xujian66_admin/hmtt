package com.heima.wemedia.listener;

import com.heima.model.common.constants.KafkaMessageConstants;
import com.heima.model.wemedia.entity.WmNews;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.scan.service.WmNewsAutoScanService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/17 11:13
 */
@Component
@Slf4j
public class NewsAutoScanListener {
    @Resource
    private WmNewsMapper wmNewsMapper;
    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;

    @KafkaListener(topics = KafkaMessageConstants.WM_NEWS_AUTO_SCAN_TOPIC,groupId = "WM_NEWS_AUTO_SCAN")
    public void wmNewsAutoScan(ConsumerRecord<String,String> record){
        String value=record.value();
        log.info("接收到消息 , 消息内容:{}", record.value());
        if (StringUtils.isNotBlank(value)) {
            log.info("开始进行文章自动审核 , 文章ID:{}", value);
            WmNews wmNews = wmNewsMapper.selectById(Long.valueOf(value));
            if (wmNews!=null) {
                wmNewsAutoScanService.newsAutoScan(wmNews);
            }

            log.info("文章自动审核完成 , 文章ID:{}", value);
        }
    }
}
