package com.heima.wemedia.scan.runner;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.model.wemedia.entity.WmSensitive;
import com.heima.utils.common.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xujian
 * @date 2023/6/15 20:03
 */
@Component
@Slf4j
public class SensitiveApplicationRunner implements ApplicationRunner {

    @Resource
    private WmSensitiveMapper wmSensitiveMapper;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        //1. 从数据看中查询敏感词列表
        List<WmSensitive> sensitiveList = wmSensitiveMapper.selectList(Wrappers.emptyWrapper());
        if (CollectionUtils.isEmpty(sensitiveList)) {
            return;
        }
        List<String> words = sensitiveList.stream().map(sensitive -> sensitive.getSensitives()).collect(Collectors.toList());
        //2. 初始化DFA数据结构
        SensitiveWordUtil.initMap(words);
    }
}
