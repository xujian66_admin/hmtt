package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmUser;

/**
 * 自媒体用户表(WmUser)表数据库访问层
 *
 * @author makejava
 * @since 2022-06-15 11:59:56
 */
public interface WmUserDao extends BaseMapper<WmUser> {

}
