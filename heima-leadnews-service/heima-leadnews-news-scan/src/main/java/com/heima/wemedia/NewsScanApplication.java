package com.heima.wemedia;

import com.hei.feign.client.ApArticleFeignClient;
import com.hei.feign.client.TaskFeignClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author xujian
 * @date 2023/6/15 16:38
 */
@SpringBootApplication
@MapperScan(basePackages = "com.heima.wemedia.mapper")
@EnableFeignClients(clients= {ApArticleFeignClient.class, TaskFeignClient.class})

public class NewsScanApplication {
    public static void main(String[] args) {
        SpringApplication.run(NewsScanApplication.class,args);
    }
}
