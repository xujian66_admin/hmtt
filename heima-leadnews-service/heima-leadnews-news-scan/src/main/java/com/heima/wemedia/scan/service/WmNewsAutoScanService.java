package com.heima.wemedia.scan.service;

import com.heima.model.wemedia.entity.WmNews;

/**
 * @author xujian
 * @date 2023/6/15 16:45
 */
public interface WmNewsAutoScanService {
    /**
     * 自媒体文章自动审核
     *
     * @param wmNews
     */
    public void newsAutoScan(WmNews wmNews);
}
