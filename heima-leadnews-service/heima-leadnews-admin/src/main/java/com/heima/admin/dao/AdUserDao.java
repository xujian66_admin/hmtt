package com.heima.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.entity.AdUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 管理员用户信息表(AdUser)表数据库访问层
 *
 * @author makejava
 * @since 2022-04-22 14:38:45
 */
public interface AdUserDao extends BaseMapper<AdUser> {

}

