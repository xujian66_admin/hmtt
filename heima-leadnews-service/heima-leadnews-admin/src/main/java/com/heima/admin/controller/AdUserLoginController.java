package com.heima.admin.controller;

import com.heima.admin.service.AdUserService;
import com.heima.model.admin.dto.AdUserDto;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xu
 */
@RestController
@RequestMapping(path = "/login")
@Api(value = "自媒体登录",tags = "自媒体" ,description = "自媒体登录API")
public class AdUserLoginController {

    @Resource
    private AdUserService adUserService;

    @PostMapping(path = "/in")
    @ApiOperation("自媒体用户登录接口")
    public ResponseResult login(@RequestBody AdUserDto dto){
        return adUserService.login(dto);
    }
}
