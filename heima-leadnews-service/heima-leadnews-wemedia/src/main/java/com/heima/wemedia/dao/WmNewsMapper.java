package com.heima.wemedia.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmNews;
import com.heima.model.wemedia.vo.NewsVo;

import java.util.List;
import java.util.Map;

/**
 * 自媒体文章表(WmNews)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
public interface WmNewsMapper extends BaseMapper<WmNews> {

    List<NewsVo> PageQuery(Map<String, Object> queryParam);
}

