package com.heima.wemedia.task;

import com.hei.feign.client.ApArticleFeignClient;
import com.hei.feign.client.TaskFeignClient;
import com.heima.common.exception.CustomException;
import com.heima.model.common.constants.TaskTypeEnum;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.schedule.pojo.TaskInfo;
import com.heima.model.wemedia.dto.ArticleDto;
import com.heima.model.wemedia.entity.WmChannel;
import com.heima.model.wemedia.entity.WmNews;
import com.heima.model.wemedia.entity.WmUser;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.dao.WmChannelMapper;
import com.heima.wemedia.dao.WmNewsMapper;
import com.heima.wemedia.dao.WmUserDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author Administrator
 * @Date 2023/6/21
 **/
@Component
@Slf4j
@Async(value = "taskExecutor")
public class WmNewsAutoPublishTask {
    @Resource
    private TaskFeignClient taskFeignClient;

    @Resource
    private ApArticleFeignClient articleFeignClient;

    @Resource
    private WmNewsMapper wmNewsMapper;

    @Resource
    private WmUserDao wmUserDao;

    @Resource
    private WmChannelMapper wmChannelMapper;

    @Scheduled(fixedDelay = 2000)
    public void wmNewsAutoPublish() {
        log.info("拉取任务自动发布文章开始执行-------------");
        while (true) {
            //1. 调用任务调度服务拉取任务
            ResponseResult<TaskInfo> result = taskFeignClient.poll(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType(), TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
            log.info("拉取任务成功 , 任务数据:{}", result.getData());
            if (result.getCode() != 200 || result.getData() == null) {
                break;
            }
            log.info("拉取到任务, 开始自动发布文章,任务数据:{}", result.getData());
            //2. 获取任务参数
            TaskInfo taskInfo = result.getData();
            WmNews param = ProtostuffUtil.deserialize(taskInfo.getParameters(), WmNews.class);
            //3. 查询文章详情
            WmNews wmNews = wmNewsMapper.selectById(param.getId());
            log.info("查询到需要发布的文章数据 , 文章id:{}", wmNews.getId());
            if (wmNews == null) {
                continue;
            }
            //4. 发布文章到APP端
            saveApArticle(wmNews);
            log.info("发布文章到APP端成功---------------------");
            //5. 修改文章状态为审核通过已发布
            wmNews.setStatus(9);
            wmNewsMapper.updateById(wmNews);
            log.info("文章发布成功,修改文章状态为审核通过已发布---------------------");
        }
    }

    /**
     * 保存自媒体文章数据到APP端
     *
     * @param wmNews
     */
    private void saveApArticle(WmNews wmNews) {
        ArticleDto apArticleDto = new ArticleDto();
        BeanUtils.copyProperties(wmNews, apArticleDto, "id");
        // newsId authorId authorName channelName layout
        apArticleDto.setNewsId(wmNews.getId());
        //封装作者信息
        apArticleDto.setAuthorId(wmNews.getUserId());
        WmUser wmUser = wmUserDao.selectById(wmNews.getUserId());
        if (wmUser != null) {
            apArticleDto.setAuthorName(wmUser.getNickname());
        }
        //封装频道信息
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if (wmChannel != null) {
            apArticleDto.setChannelName(wmChannel.getName());
        }
        apArticleDto.setLayout(wmNews.getType());

        ResponseResult<String> result = articleFeignClient.save(apArticleDto);
        if (result.getCode() != 200) {
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
    }
}