package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.entity.WmUser;
import com.heima.wemedia.service.WmUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/26 18:23
 */
@RestController
@RequestMapping(path = "/api/v1/user")
public class WmUserController {

    @Resource
    private WmUserService wmUserService;

    @PostMapping(path = "/save")
    public ResponseResult save(@RequestBody WmUser wmUser){
        return wmUserService.saveUser(wmUser);
    }
}
