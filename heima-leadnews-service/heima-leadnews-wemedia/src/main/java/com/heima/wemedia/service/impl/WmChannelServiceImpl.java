package com.heima.wemedia.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDto;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dto.AdChannel;
import com.heima.model.wemedia.dto.ChannelDto;
import com.heima.wemedia.dao.WmChannelMapper;
import com.heima.model.wemedia.entity.WmChannel;
import com.heima.wemedia.service.WmChannelService;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 自媒体频道表(WmChannel)表服务实现类
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
@Service("wmChannelService")
public class  WmChannelServiceImpl extends ServiceImpl<WmChannelMapper, WmChannel> implements WmChannelService {

    @Resource
    private WmChannelMapper wmChannelMapper;
    @Override
    public ResponseResult channels() {
        LambdaQueryWrapper<WmChannel> wrapper = Wrappers.<WmChannel>lambdaQuery()
                .eq(WmChannel::getStatus, 1)
                .orderByAsc(WmChannel::getOrd);
        List<WmChannel> channelList = wmChannelMapper.selectList(wrapper);
        return ResponseResult.okResult(channelList);
    }

    @Override
    public ResponseResult channelList(ChannelDto channelDto) {
        channelDto.checkParam();
        LambdaQueryWrapper<WmChannel> wrapper = Wrappers.<WmChannel>lambdaQuery().like(WmChannel::getName, channelDto.getName()).eq(WmChannel::getStatus,1);
        Page<WmChannel> page = wmChannelMapper.selectPage(new Page<>(channelDto.getPage(), channelDto.getSize()), wrapper);
        PageResponseResult result = new PageResponseResult((int)page.getCurrent(), (int)page.getSize(), (int)page.getTotal());
        result.setData(page.getRecords());
        return result;
    }

    @Override
    public ResponseResult del(Integer id) {
        //校验参数
        if (id==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        LambdaQueryWrapper<WmChannel> w= Wrappers.<WmChannel>lambdaQuery().eq(WmChannel::getId, id);
        WmChannel wmChannel=new WmChannel();
        wmChannel.setStatus(2);
        wmChannelMapper.update(wmChannel,w);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult saveChannel(AdChannel adChannel) {
        //校验参数
        if (adChannel==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        WmChannel wmChannel=new WmChannel();
        BeanUtil.copyProperties(adChannel,wmChannel);
        if (adChannel.getIsDefault()!=null&&adChannel.getIsDefault()==true) {
            wmChannel.setIsDefault(1);
        }else {
            wmChannel.setIsDefault(0);
        }
        if (adChannel.getStatus()!=null&&adChannel.getStatus()==true) {
            wmChannel.setStatus(1);
        }else {
            wmChannel.setStatus(0);
        }
        wmChannelMapper.insert(wmChannel);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult updateChannel(AdChannel adChannel) {
        //校验参数
        if (adChannel==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        WmChannel wmChannel=new WmChannel();
        BeanUtil.copyProperties(adChannel,wmChannel);
        if (adChannel.getIsDefault()!=null&&adChannel.getIsDefault()==true) {
            wmChannel.setIsDefault(1);
        }else {
            wmChannel.setIsDefault(0);
        }
        if (adChannel.getStatus()!=null&&adChannel.getStatus()==true) {
            wmChannel.setStatus(1);
        }else {
            wmChannel.setStatus(0);
        }
        LambdaQueryWrapper<WmChannel> wrapper = Wrappers.<WmChannel>lambdaQuery().eq(WmChannel::getId, wmChannel.getId());
        wmChannelMapper.update(wmChannel,wrapper);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

    }
}

