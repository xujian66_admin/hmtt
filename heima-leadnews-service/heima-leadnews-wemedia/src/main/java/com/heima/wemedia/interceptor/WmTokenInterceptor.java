package com.heima.wemedia.interceptor;

import com.alibaba.fastjson.JSON;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserinfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.entity.WmUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class WmTokenInterceptor extends HandlerInterceptorAdapter {

    /**
     * 前置拦截 -  获取请求头中的  userId , 放入到当前线程中
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1. 获取请求头
        String userId = request.getHeader("userId");
        if (StringUtils.isNotBlank(userId)) {
            //2. 存入线程
            UserInfo userInfo=new UserInfo();
            userInfo.setUserId(Long.valueOf(userId));

            UserinfoThreadLocalUtil.setUser(userInfo);
            return super.preHandle(request, response, handler);
        }

        //没有权限
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        ResponseResult responseResult = ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(responseResult));

        return false;
    }

    /**
     * 清除线程中的用户数据
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //清除线程数据
        UserinfoThreadLocalUtil.clear();
        super.postHandle(request, response, handler, modelAndView);
    }
}