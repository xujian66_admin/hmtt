package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.WmUserLoginDto;
import com.heima.wemedia.service.WmUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/login")
@Api(value = "用户模块接口",tags = "用户模块")
public class WmUserLoginController {

    @Resource
    private WmUserService wmUserService;

    @ApiOperation(value = "用户登录接口")
    @PostMapping(path = "/in")
    public ResponseResult login(@RequestBody WmUserLoginDto dto){
        return wmUserService.login(dto);
    }

}
