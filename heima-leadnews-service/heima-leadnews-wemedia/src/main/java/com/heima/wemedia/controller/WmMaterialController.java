package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.WmMaterialDto;
import com.heima.wemedia.service.WmMaterialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author xujian
 * @date 2023/6/11 22:04
 */
@RestController
@RequestMapping("/api/v1/material")
@Api(value = "自媒体素材管理API", tags = "自媒体素材管理")
public class WmMaterialController {
    @Resource
    private WmMaterialService wmMaterialService;

    /**
     * 上传素材
     *
     * @param multipartFile
     * @return
     * @throws IOException
     */
    @ApiOperation(value = "素材上传")
    @PostMapping(path = "/upload_picture")
    public ResponseResult uploadPic(MultipartFile multipartFile) throws IOException {
        return wmMaterialService.upload(multipartFile);
    }

    /**
     * 查询素材列表
     *
     * @param dto
     * @return
     */
    @ApiOperation(value = "素材列表查询")
    @PostMapping(path = "/list")
    public ResponseResult list(@RequestBody WmMaterialDto dto) {
        return wmMaterialService.queryList(dto);
    }

    /**
     * 收藏素材查询
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "收藏素材查询")
    @DeleteMapping(path = "del_picture/{id}")
    public ResponseResult delete(@PathVariable Integer id) {
        return wmMaterialService.deletePic(id);
    }

    /**
     * 收藏
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "收藏素材")
    @PutMapping(path = "collect/{id}")
    public ResponseResult collect(@PathVariable Integer id) {
        return wmMaterialService.collect(id);
    }

    /**
     * 取消收藏
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "取消收藏")
    @PutMapping(path = "cancel_collect/{id}")
    public ResponseResult cancelCollect(@PathVariable Integer id) {
        return wmMaterialService.cancelCollect(id);
    }
}
