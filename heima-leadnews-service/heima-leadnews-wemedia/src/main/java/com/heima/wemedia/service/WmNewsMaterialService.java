package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.wemedia.entity.WmNewsMaterial;

/**
 * 文章素材关联表(WmNewsMaterial)表服务接口
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
public interface WmNewsMaterialService extends IService<WmNewsMaterial> {

}

