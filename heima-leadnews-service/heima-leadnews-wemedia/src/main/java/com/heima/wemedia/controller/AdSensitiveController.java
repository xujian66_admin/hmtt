package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.AdSensitive;
import com.heima.model.wemedia.dto.AdSensitiveDto;
import com.heima.model.wemedia.entity.WmSensitive;
import com.heima.wemedia.service.AdSensitiveService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/25 10:52
 */
@RestController
@RequestMapping("/api/v1/sensitive")
public class AdSensitiveController {
    @Resource
    private AdSensitiveService adSensitiveService;
    @PostMapping(path = "/save")
    public ResponseResult saveSensitive(@RequestBody AdSensitive adSensitive){
        return adSensitiveService.save(adSensitive);
    }

    @PostMapping(path = "/list")
    public ResponseResult queryList(@RequestBody AdSensitiveDto adSensitiveDto){
        return adSensitiveService.list(adSensitiveDto);
    }
    @PostMapping("/update")
    public ResponseResult update(@RequestBody WmSensitive wmSensitive){
        return adSensitiveService.update(wmSensitive);
    }

    @DeleteMapping("/del/{id}")
    public ResponseResult deleteById(@PathVariable Integer id){
        return adSensitiveService.del(id);
    }
}
