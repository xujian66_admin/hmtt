package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dto.WmUserLoginDto;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import com.heima.wemedia.dao.WmUserDao;
import com.heima.model.wemedia.entity.WmUser;
import com.heima.wemedia.service.WmUserService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 自媒体用户表(WmUser)表服务实现类
 *
 * @author makejava
 * @since 2022-06-15 11:59:58
 */
@Service("wmUserService")
public class WmUserServiceImpl extends ServiceImpl<WmUserDao, WmUser> implements WmUserService {

    @Resource
    private WmUserDao wmUserDao;

    @Override
    public ResponseResult login(WmUserLoginDto dto) {
        //1. 校验参数
        if (StringUtils.isEmpty(dto.getName()) || StringUtils.isEmpty(dto.getPassword())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
        }
        //2. 根据用户名查询用户信息
        WmUser wmUser = wmUserDao.selectOne(Wrappers.<WmUser>lambdaQuery().eq(WmUser::getUsername, dto.getName()));
        if (wmUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        if (wmUser.getStatus()!=1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH,"用户状态未启用");
        }

        //获取密文

        //3. 根据返回用户信息中的盐 和 用户输入的密码一起进行MD5加密得到密文
        String encode = MD5Utils.encodeWithSalt(dto.getPassword(), wmUser.getSalt());
        //4. 比对加密出来的密文和数据库保存的密文
        if (!StringUtils.equals(encode, wmUser.getPassword())) {
            //4.1 密文不匹配 : 密码错误 , 返回错误信息
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
        }

        //4.2 密文匹配 : 密码正确 , 登录成功 , 生成token返回到客户端
        String token = AppJwtUtil.getToken(wmUser.getId().longValue());

        Map<String, Object> data = new HashMap<>();
        data.put("token", token);

        wmUser.setSalt(null);
        wmUser.setPassword(null);
        data.put("user", wmUser);

        return ResponseResult.okResult(data);
    }

    @GlobalTransactional
    @Override
    public ResponseResult saveUser(WmUser wmUser) {
        //校验参数
        if (wmUser==null||wmUser.getSalt()==null||wmUser.getPassword()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        wmUserDao.insert(wmUser);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


}
