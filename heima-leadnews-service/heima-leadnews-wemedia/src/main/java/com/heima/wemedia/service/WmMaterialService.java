package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.WmMaterialDto;
import com.heima.model.wemedia.entity.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author xujian
 * @date 2023/6/11 22:07
 */
public interface WmMaterialService  extends IService<WmMaterial>{
    ResponseResult upload(MultipartFile multipartFile);

    ResponseResult queryList(WmMaterialDto dto);

    ResponseResult deletePic(Integer id);

    ResponseResult collect(Integer id);

    ResponseResult cancelCollect(Integer id);
}
