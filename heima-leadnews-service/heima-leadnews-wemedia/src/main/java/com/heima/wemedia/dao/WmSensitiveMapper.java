package com.heima.wemedia.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmSensitive;

/**
 * 自媒体敏感词表(WmSensitive)表数据库访问层
 *
 * @author Xujian
 * @since 2023-06-15 19:31:06
 */
public interface WmSensitiveMapper extends BaseMapper<WmSensitive> {

}

