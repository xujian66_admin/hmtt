package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.AdChannel;
import com.heima.model.wemedia.dto.ChannelDto;
import com.heima.model.wemedia.entity.WmChannel;

/**
 * 自媒体频道表(WmChannel)表服务接口
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
public interface WmChannelService extends IService<WmChannel> {

    ResponseResult channels();

    ResponseResult channelList(ChannelDto channelDto);

    ResponseResult del(Integer id);

    ResponseResult saveChannel(AdChannel adChannel);

    ResponseResult updateChannel(AdChannel adChannel);
}

