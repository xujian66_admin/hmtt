package com.heima.wemedia.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dto.AdChannel;
import com.heima.model.wemedia.dto.ChannelDto;
import com.heima.model.wemedia.entity.WmChannel;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 自媒体频道表(WmChannel)表控制层
 *
 * @author makejava
 * @since 2023-06-14 09:21:38
 */
@RestController
@RequestMapping(path = "/api/v1/channel")
public class WmChannelController {
    /**
     * 服务对象
     */
    @Resource
    private WmChannelService wmChannelService;

    @GetMapping(path = "/channels")
    public ResponseResult channels() {
        return wmChannelService.channels();
    }

    @PostMapping("/list")
    public ResponseResult channelList(@RequestBody ChannelDto channelDto){
        return wmChannelService.channelList(channelDto);
    }

    /**
     * 删除频道
     * @param id
     * @return
     */
    @DeleteMapping(path = "/del/{id}")
    public ResponseResult del(@PathVariable Integer id){
        return wmChannelService.del(id);
    }

    /**
     * 保存频道
     * @param adChannel
     * @return
     */
    @PostMapping(path = "/save")
    public ResponseResult save(@RequestBody AdChannel adChannel){
        return wmChannelService.saveChannel(adChannel);
    }

    @PostMapping(path = "/update")
    public ResponseResult update(@RequestBody AdChannel adChannel){

        return wmChannelService.updateChannel(adChannel);
    }

}

