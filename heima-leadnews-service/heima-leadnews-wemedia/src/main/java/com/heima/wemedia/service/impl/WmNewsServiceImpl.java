package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hei.feign.client.TaskFeignClient;
import com.heima.common.exception.CustomException;
import com.heima.common.thread.UserinfoThreadLocalUtil;
import com.heima.model.common.constants.KafkaMessageConstants;
import com.heima.model.common.constants.TaskTypeEnum;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.wemedia.dto.NewsAuthDto;
import com.heima.model.wemedia.dto.WmNewsDownOrUpDto;
import com.heima.model.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dto.WmNewsDto;
import com.heima.model.wemedia.entity.WmMaterial;
import com.heima.model.wemedia.entity.WmNewsMaterial;
import com.heima.model.wemedia.vo.NewsVo;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.dao.WmMaterialDao;
import com.heima.wemedia.dao.WmNewsMapper;
import com.heima.model.wemedia.entity.WmNews;
import com.heima.wemedia.dao.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmNewsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 自媒体文章表(WmNews)表服务实现类
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
@Service("wmNewsService")
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {

    @Resource
    private WmMaterialDao wmMaterialDao;
    @Resource
    private WmNewsMapper wmNewsMapper;
    @Resource
    private WmNewsMaterialMapper wmNewsMaterialMapper;
    @Resource
    private KafkaTemplate kafkaTemplate;
    @Resource
    private TaskFeignClient taskFeignClient;
    /**
     * 提交文章
     * @param dto
     * @return
     */
    @Override
    public ResponseResult<String> submit(WmNewsDto dto) {
        //1.校验参数
        if (dto == null || StringUtils.isEmpty(dto.getTitle())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getTitle().length() > 30) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "文章长度过长");
        }
        //2.抽取文章内容中的图片
        //2.1获取文章封面图片
        List<String> cover_images = dto.getImages();
        //2.2获取文章内容图片
        List<String> content_images= extractContentImage(dto.getContent());
        //3.判断文章类型是否自动
        Short type = dto.getType();
        //如果是自动，抽取图片作为封面，设置文章类型 0 无图，1：单图 3多图
        if(type==-1){
            if (content_images.size()>=3){
                cover_images=content_images.stream().limit(3).collect(Collectors.toList());
                type=3;
            }else if(content_images.size()>0){
                cover_images=content_images.stream().limit(1).collect(Collectors.toList());
                type=1;
            }else {
                cover_images=Collections.emptyList();
                type=0;
            }
        }
        //封装数据保存文章数据到文章表

        //封装数据保存数据到文章素材的关联表
        //5.1保存素材封面引用数据
        WmNews wmNews=saveWmNews(dto,cover_images,type);
        saveWmNewsMaterialRelation(wmNews.getId(),cover_images,content_images);
        //发送审核信息到kafka
        if(wmNews.getStatus()==1){
            kafkaTemplate.send(KafkaMessageConstants.WM_NEWS_AUTO_SCAN_TOPIC,wmNews.getId().toString());
        }

        //5.2保存素材封面引用数据
        return ResponseResult.okResult("发布文章成功");

    }

    /**
     * 分页查询文章
     * @param dto
     * @return
     */
    @Override
    public ResponseResult queryList(WmNewsPageReqDto dto) {
        //1.校验数据
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.封装分页查询条件
        dto.checkParam();
        //2.1基础条件
        LambdaQueryWrapper<WmNews> wrapper = Wrappers.<WmNews>lambdaQuery();
        //2.1.1状态 精确查询
        if(dto.getStatus()!=null){
            wrapper.eq(WmNews::getStatus,dto.getStatus());
        }
        //2.1.2 关键字 模糊查询
        if (StringUtils.isNotEmpty(dto.getKeyword())) {
            wrapper.like(WmNews::getTitle,dto.getKeyword());
        }
        //2.1.3 发布时间 范围查询
        if (dto.getBeginPubdate() !=null && dto.getEndPubdate()!=null) {
            wrapper.between(WmNews::getPublishTime,dto.getBeginPubdate(),dto.getEndPubdate());
        }
        //2.1.4 频道id 精确查询
        if (dto.getChannelId()!=null) {
            wrapper.eq(WmNews::getChannelId,dto.getChannelId());
        }
        //2.1.5 登录用户ID 精确查询
        wrapper.eq(WmNews::getUserId,UserinfoThreadLocalUtil.getUser().getUserId());
        //2.2 分页条件
        Page<WmNews> page=new Page<>(dto.getPage(),dto.getSize());
        //3.调用数据访问层查询数据
        wmNewsMapper.selectPage(page,wrapper);
        //4.封装数据返回
        PageResponseResult result=new PageResponseResult(dto.getPage(),dto.getSize(), (int) page.getTotal());
        result.setData(page.getRecords());
        return result;
    }

    /**
     * 根据id删除
     * @param newsId
     * @return
     */
    @Override
    public ResponseResult deleteById(Long newsId) {
        //校验数据
        if (newsId==null) {
            return ResponseResult.errorResult(501,"文章id不能缺失");
        }
        WmNews wmNews = wmNewsMapper.selectById(newsId);
        if (wmNews==null){
            return ResponseResult.errorResult(1002,"文章不存在");
        }
        if (wmNews.getStatus()==9){
            return ResponseResult.errorResult(501,"文章已发布不能删除");
        }
        wmNewsMapper.deleteById(newsId);
        LambdaQueryWrapper<WmNewsMaterial> wrapper = Wrappers.<WmNewsMaterial>lambdaQuery().eq(WmNewsMaterial::getNewsId, newsId);
        wmNewsMaterialMapper.delete(wrapper);
        return ResponseResult.okResult("删除文章成功");
    }



    /**
     * 修改文章上下架状态
     * @param dto
     * @return
     */
    @Override
    public ResponseResult downOrUp(WmNewsDownOrUpDto dto) {
        if (dto==null||dto.getEnable()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        WmNews wmNews=wmNewsMapper.selectById(dto.getId());
        if (wmNews==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //文章状态校验
        if(wmNews.getStatus()!=9){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"未发布文章不能上下架");
        }
        //3.修改文章配置信息
        if(dto.getEnable()!=0&&dto.getEnable()!=1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        wmNews.setEnable(dto.getEnable().intValue());
        wmNewsMapper.updateById(wmNews);

        //4. 向kafka发送消息  文章id , 状态-------------------------
        Map<String, String> data = new HashMap<>();
        data.put("newsId", wmNews.getId() + "");
        data.put("enable", dto.getEnable() + "");
        kafkaTemplate.send(KafkaMessageConstants.WM_NEWS_UP_DOWN_TOPIC, JSON.toJSONString(data));
        return ResponseResult.okResult("文章上下架成功");
    }

    /**
     * 按ID查询
     * @param id
     * @return
     */
    @Override
    public ResponseResult findById(Long id) {
        WmNews wmNews = wmNewsMapper.selectById(id);
        if (wmNews==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        return ResponseResult.okResult(wmNews);
    }

    @Override
    public ResponseResult listVo(NewsAuthDto newsAuthDto) {
        //校验参数
        newsAuthDto.checkParam();
        if (newsAuthDto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //LambdaQueryWrapper<WmNews> wrapper = Wrappers.<WmNews>lambdaQuery();
        //if (newsAuthDto.getTitle()!=null){
        //    wrapper.like(WmNews::getTitle,newsAuthDto.getTitle());
        //}
        //if (newsAuthDto.getStatus()!=null){
        //    wrapper.eq(WmNews::getStatus,newsAuthDto.getStatus());
        //}
        //Page<WmNews> page = new Page<>(newsAuthDto.getPage(), newsAuthDto.getSize());
        //wmNewsMapper.selectPage(page, wrapper);
        //PageResponseResult result = new PageResponseResult(newsAuthDto.getPage(), newsAuthDto.getSize(), (int) page.getTotal());
        //result.setData(page.getRecords());
        //return result;
        Map<String,Object> queryParam =new HashMap<>();
        queryParam.put("size",newsAuthDto.getSize());
        queryParam.put("status",newsAuthDto.getStatus());
        queryParam.put("title",newsAuthDto.getTitle());
        queryParam.put("page",newsAuthDto.getPage());
        queryParam.put("start",((newsAuthDto.getPage()-1)*newsAuthDto.getSize()));
        List<NewsVo> newsVo=wmNewsMapper.PageQuery(queryParam);
        newsVo.stream().forEach(newsVo1 -> {
            Date updatedTime = newsVo1.getUpdatedTime();
            newsVo1.setSubmitedTime(updatedTime);
        });
        return ResponseResult.okResult(newsVo);
    }

    @Override
    public ResponseResult authFail(NewsAuthDto dto) {
        //校验参数
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        LambdaQueryWrapper<WmNews> wrapper = Wrappers.<WmNews>lambdaQuery().eq(WmNews::getId, dto.getId());
        WmNews wmNews=new WmNews();
        wmNews.setStatus(2);
        wmNews.setReason(dto.getMsg());
        wmNewsMapper.update(wmNews,wrapper);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);

    }

    @Override
    public ResponseResult authPass(NewsAuthDto dto) {
        //校验参数
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        WmNews wmNews = wmNewsMapper.selectById(dto.getId());
        Date publishTime = wmNews.getPublishTime();
        Long id = wmNews.getId();
        wmNewsAutoPublish(id,publishTime);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
    private void wmNewsAutoPublish(Long id, Date publishTime) {
        TaskDto taskDto = new TaskDto();
        taskDto.setTaskType(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType());
        taskDto.setPriority(TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        taskDto.setExecuteTime(publishTime.getTime());

        //修改之后的实体信息只有id
        WmNews wmNews = new WmNews();
        wmNews.setId(id);
        taskDto.setParameters(ProtostuffUtil.serialize(wmNews));

        ResponseResult<Long> responseResult = taskFeignClient.addTask(taskDto);
        if (responseResult.getCode().intValue() != 200) {
            throw new RuntimeException("文章定时发布任务保存失败");
        }
    }
    private void saveWmNewsMaterialRelation(Long id,List<String> cover_images,List<String> content_images){
        //1.保存封面素材引用
        if(!CollectionUtils.isEmpty(cover_images)){
            saveNewsImagesRelation(id,cover_images,0);
        }
        //2.保存内容素材引用
        if(!CollectionUtils.isEmpty(content_images)){
            saveNewsImagesRelation(id,content_images,1);
        }
    }

    /**
     * 保存图片引用到数据库
     *
     * @param images
     * @param newsId
     * @param type
     */
    private void saveNewsImagesRelation(Long newsId,List<String> images, Integer type) {
        //如果需要保存的图片集合中没有图片不需要保存

        //对图片的url进行去重操作
        images = images.stream().distinct().collect(Collectors.toList());
        List<WmMaterial> wmMaterialList = wmMaterialDao.selectList(Wrappers.<WmMaterial>lambdaQuery().in(WmMaterial::getUrl, images));
        //List<Long> ids = wmMaterialList.stream().map(wmMaterial -> wmMaterial.getId()).collect(Collectors.toList());
        //判断是否存在缺失素材
        if (images.size() > wmMaterialList.size()) {
            throw new CustomException(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //封装数据保存
        //for (Long materialId : ids) {
        //    WmNewsMaterial wmNewsMaterial = new WmNewsMaterial();
        //    wmNewsMaterial.setNewsId(newsId);
        //    wmNewsMaterial.setMaterialId(materialId);
        //    wmNewsMaterial.setType(type);
        //    wmNewsMaterialMapper.insert(wmNewsMaterial);
        //}

        //批量保存素材关系数据到数据库
        List<WmNewsMaterial> wmNewsMaterialList = wmMaterialList.stream().map(wmMaterial -> {
            WmNewsMaterial wmNewsMaterial = new WmNewsMaterial();
            wmNewsMaterial.setNewsId(newsId);
            wmNewsMaterial.setMaterialId(wmMaterial.getId());
            wmNewsMaterial.setType(type);
            return wmNewsMaterial;
        }).collect(Collectors.toList());


        wmNewsMaterialMapper.saveNewsMaterialRelation(wmNewsMaterialList);

    }

    private WmNews saveWmNews(WmNewsDto wmNewsDto, List<String> cover_Images, Short type) {
        //判断当前用户操作行为
        //封装自媒体文章实体数据
        WmNews wmNews = new WmNews();
        BeanUtils.copyProperties(wmNewsDto, wmNews, "id");
        wmNews.setImages(StringUtils.join(cover_Images, ","));
        wmNews.setUserId(UserinfoThreadLocalUtil.getUser().getUserId());
        wmNews.setType(type.intValue());
        wmNews.setStatus(wmNewsDto.getStatus().intValue());
        wmNews.setEnable(1);
        //文章ID为空,表示新增
        if(wmNewsDto.getId()==null){
            wmNewsMapper.insert(wmNews);
            return wmNews;
        }
        //文章id不为空表示修改
        wmNews.setId(wmNewsDto.getId());
        wmNewsMapper.updateById(wmNews);
        wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery().eq(WmNewsMaterial::getNewsId,wmNews.getId()));
        return wmNews;
    }

    /**
     * 抽取文章内容中的图片数据
     * @param content
     * @return
     */
    private List<String> extractContentImage(String content){
        //如果内容为空，直接返回空集合
        if (StringUtils.isEmpty(content)) {
            return Collections.emptyList();
        }
        //如果内容不为空，抽取内容中的图片URL列表
        List<Map> mapList = JSON.parseArray(content, Map.class);
        //List<String> images=new ArrayList<>();
        ////遍历获取图片URL
        //for (Map map : mapList) {
        //    if("image".equals(map.get("type"))){
        //        String value = map.get("value").toString();
        //        images.add(value);
        //    }
        //}
        List<String> list=mapList.stream()
                .filter(map -> "image".equals(map.get("type")))
                .map(map -> map.get("value").toString())
                .collect(Collectors.toList());
        return list;
    }

}

