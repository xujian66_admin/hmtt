package com.heima.wemedia.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmMaterial;

/**
 * @author xujian
 * @date 2023/6/12 11:50
 */
public interface WmMaterialDao extends BaseMapper<WmMaterial> {
}
