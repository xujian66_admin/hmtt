package com.heima.wemedia.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.entity.WmNewsMaterial;

import java.util.List;

/**
 * 文章素材关联表(WmNewsMaterial)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {

    void saveNewsMaterialRelation(List<WmNewsMaterial> wmNewsMaterialList);
}

