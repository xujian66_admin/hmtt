package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.WmUserLoginDto;
import com.heima.model.wemedia.entity.WmUser;

/**
 * 自媒体用户表(WmUser)表服务接口
 *
 * @author makejava
 * @since 2022-06-15 11:59:57
 */
public interface WmUserService extends IService<WmUser> {

    /**
     * 自媒体端用户登录
     *
     * @param dto
     * @return
     */
    ResponseResult login(WmUserLoginDto dto);

    ResponseResult saveUser(WmUser wmUser);
}
