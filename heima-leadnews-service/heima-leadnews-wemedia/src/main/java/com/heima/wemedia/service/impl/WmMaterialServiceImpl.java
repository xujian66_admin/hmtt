package com.heima.wemedia.service.impl;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.thread.UserinfoThreadLocalUtil;
import com.heima.file.MinIOTemplate;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.WmMaterialDto;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.entity.WmMaterial;
import com.heima.wemedia.dao.WmMaterialDao;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * @author xujian
 * @date 2023/6/12 11:31
 */
@Service("wmMaterialService")
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialDao,WmMaterial> implements WmMaterialService {
    @Resource
    private MinIOTemplate minIOTemplate;
    @Resource
    private WmMaterialDao wmMaterialDao;

    /**
     * 文章素材上传
     * @param multipartFile
     * @return
     */
    @Override
    public ResponseResult upload(MultipartFile multipartFile) {
        if(multipartFile==null||multipartFile.isEmpty()){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.上传文件到minio
        //2.1获取唯一文件名称uuid
        String originalFilename = multipartFile.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String filename = UUID.randomUUID().toString().replace("-", "") + suffix;
        String url=null;
        try {
            url=minIOTemplate.uploadImgFile("",filename,multipartFile.getInputStream());
        }catch (IOException e){
            e.printStackTrace();
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"素材上传失败");
        }
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setUserId(UserinfoThreadLocalUtil.getUser().getUserId().intValue());
        wmMaterial.setUrl(url);
        wmMaterial.setIsCollection(0);
        wmMaterialDao.insert(wmMaterial);
        return ResponseResult.okResult(wmMaterial);

    }

    /**
     * 素材分页查询
     * @param dto
     * @return
     */
    @Override
    public ResponseResult queryList(WmMaterialDto dto) {
        //校验参数
        dto.checkParam();
        //2.封装分页查询条件，执行查询
        LambdaQueryWrapper<WmMaterial> wrapper = Wrappers.<WmMaterial>lambdaQuery();
        wrapper.eq(WmMaterial::getUserId, UserinfoThreadLocalUtil.getUser().getUserId());
        if(dto.getIsCollection()!=null&&dto.getIsCollection()==1){
            wrapper.eq(WmMaterial::getIsCollection,dto.getIsCollection());
        }
        //3.分页操作
        Page<WmMaterial> page=wmMaterialDao.selectPage(new Page<>(dto.getPage(),dto.getSize()),wrapper);
        PageResponseResult result = new PageResponseResult((int)page.getCurrent(), (int)page.getSize(), (int)page.getTotal());
        result.setData(page.getRecords());
        return result;

    }

    /**
     * 删除素材
     * @param id
     * @return
     */
    @Transactional
    @Override
    public ResponseResult deletePic(Integer id) {
        //校验参数是否正确
        if (id==-1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"不存在此图片");
        }
        //查找数据库
        WmMaterial wmMaterial = wmMaterialDao.selectById(id);
        if (wmMaterial==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST,"图片不存在");
        }
        //删除数据库中的数据
        try {
            wmMaterialDao.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.errorResult(501,"文件删除失败");
        }
        minIOTemplate.delete(wmMaterial.getUrl());
        return ResponseResult.okResult(200,"操作成功");
    }

    /**
     * 收藏素材
     */
    @Override
    public ResponseResult collect(Integer id) {
        WmMaterial wmMaterial = wmMaterialDao.selectById(id);
        if (wmMaterial==null) {
            ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        wmMaterial.setIsCollection(1);
        wmMaterialDao.updateById(wmMaterial);
        return ResponseResult.okResult(200,"操作成功");
    }

    /**
     * 取消收藏
     * @param id
     * @return
     */
    @Override
    public ResponseResult cancelCollect(Integer id) {
        WmMaterial wmMaterial = wmMaterialDao.selectById(id);
        if (wmMaterial==null) {
            ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        wmMaterial.setIsCollection(0);
        wmMaterialDao.updateById(wmMaterial);
        return ResponseResult.okResult(200,"操作成功");
    }
}
