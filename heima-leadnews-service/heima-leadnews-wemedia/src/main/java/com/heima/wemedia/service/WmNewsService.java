package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.NewsAuthDto;
import com.heima.model.wemedia.dto.WmNewsDownOrUpDto;
import com.heima.model.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.wemedia.dto.WmNewsDto;
import com.heima.model.wemedia.entity.WmNews;

/**
 * 自媒体文章表(WmNews)表服务接口
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
public interface WmNewsService extends IService<WmNews> {

    ResponseResult<String> submit(WmNewsDto dto);

    ResponseResult queryList(WmNewsPageReqDto dto);

    ResponseResult deleteById(Long newsId);

    ResponseResult downOrUp(WmNewsDownOrUpDto dto);

    ResponseResult findById(Long id);

    ResponseResult listVo(NewsAuthDto newsAuthDto);

    ResponseResult authFail(NewsAuthDto dto);

    ResponseResult authPass(NewsAuthDto dto);
}

