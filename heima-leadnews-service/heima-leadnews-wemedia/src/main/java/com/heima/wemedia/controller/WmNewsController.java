package com.heima.wemedia.controller;



import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.NewsAuthDto;
import com.heima.model.wemedia.dto.WmNewsDownOrUpDto;
import com.heima.model.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.wemedia.dto.WmNewsDto;
import com.heima.model.wemedia.entity.WmNews;
import com.heima.wemedia.service.WmNewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 自媒体文章表(WmNews)表控制层
 *
 * @author makejava
 * @since 2023-06-14 09:21:50
 */
@RestController
@RequestMapping("/api/v1/news")
@Api(value = "自媒体文章管理",tags = "自媒体文章管理")
public class WmNewsController  {
    /**
     * 服务对象
     */
    @Resource
    private WmNewsService wmNewsService;

    /**
     * 自媒体文章发布
     * @param dto
     * @return
     */
    @PostMapping(path = "/submit")
    @ApiOperation("自媒体文章发布")
    public ResponseResult<String> submit(@RequestBody WmNewsDto dto){
        return wmNewsService.submit(dto);
    }

    /**
     * 文章查询
     * @param dto
     * @return
     */
    @ApiOperation("文章查询")
    @PostMapping(path = "/list")
    public ResponseResult list(@RequestBody WmNewsPageReqDto dto){
        return wmNewsService.queryList(dto);
    }

    /**
     * 删除文章
     * @param newsId
     * @return
     */
    @ApiOperation("删除文章接口")
    @GetMapping(path = "del_news/{newsId}")
    public ResponseResult delete(@PathVariable Long newsId){
        return wmNewsService.deleteById(newsId);
    }

    @ApiOperation("上下架操作")
    @PostMapping(path = "/down_or_up")
    public ResponseResult downOrUp(@RequestBody WmNewsDownOrUpDto dto){
        return wmNewsService.downOrUp(dto);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping(path = "/one/{id}")
    public ResponseResult getById(@PathVariable Long id){
        return wmNewsService.findById(id);
    }

    /**
     * 管理端文章页面展示
     * @param newsAuthDto
     * @return
     */
    @PostMapping(path = "/list_vo")
    public ResponseResult listVo(@RequestBody NewsAuthDto newsAuthDto){
        return wmNewsService.listVo(newsAuthDto);
    }

    /**
     * 管理端文章详情展示
     * @param id
     * @return
     */
    @GetMapping(path = "/one_vo/{id}")
    public ResponseResult<WmNews> getOne(@PathVariable Integer id){
        LambdaQueryWrapper<WmNews> wrapper = Wrappers.<WmNews>lambdaQuery().eq(WmNews::getId, id);
        WmNews one = wmNewsService.getOne(wrapper);
        return ResponseResult.okResult(one);
    }

    /**
     * 人工审核未通过
     * @param dto
     * @return
     */
    @PostMapping(path = "/auth_fail")
    public ResponseResult authFail(@RequestBody NewsAuthDto dto){
        return wmNewsService.authFail(dto);
    }

    /**
     * 人工审核通过
     * @param dto
     * @return
     */
    @PostMapping(path = "/auth_pass")
    public ResponseResult authPass(@RequestBody NewsAuthDto dto){
        return wmNewsService.authPass(dto);
    }

}

