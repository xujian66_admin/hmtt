package com.heima.wemedia.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.AdSensitiveDto;
import com.heima.model.wemedia.entity.WmSensitive;

/**
 * @author xujian
 * @date 2023/6/25 10:56
 */
public interface AdSensitiveService {
    ResponseResult save(WmSensitive adSensitive);

    ResponseResult list(AdSensitiveDto adSensitiveDto);

    ResponseResult update(WmSensitive wmSensitive);

    ResponseResult del(Integer id);
}
