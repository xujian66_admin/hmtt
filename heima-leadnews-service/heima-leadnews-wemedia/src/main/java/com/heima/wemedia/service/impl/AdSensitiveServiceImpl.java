package com.heima.wemedia.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.heima.common.exception.CustomException;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dto.AdSensitiveDto;
import com.heima.model.wemedia.entity.WmSensitive;
import com.heima.wemedia.dao.WmSensitiveMapper;
import com.heima.wemedia.service.AdSensitiveService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author xujian
 * @date 2023/6/25 10:56
 */
@Service
public class AdSensitiveServiceImpl implements AdSensitiveService {

    @Resource
    private WmSensitiveMapper wmSensitiveMapper;

    @Override
    public ResponseResult save(WmSensitive adSensitive) {
        //校验参数
        if (adSensitive==null) {
            return  ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //查询判断是否存在
        LambdaQueryWrapper<WmSensitive> wrapper = Wrappers.<WmSensitive>lambdaQuery().eq(WmSensitive::getSensitives, adSensitive.getSensitives());
        WmSensitive wmSensitive = wmSensitiveMapper.selectOne(wrapper);
        if (wmSensitive!=null){
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
        adSensitive.setUpdatedTime(new Date());
        wmSensitiveMapper.insert(adSensitive);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult list(AdSensitiveDto adSensitiveDto) {
        //校验参数
        adSensitiveDto.checkParam();
        //if (adSensitiveDto==null) {
        //    return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        //}
        LambdaQueryWrapper<WmSensitive> wrapper = Wrappers.<WmSensitive>lambdaQuery();
        Page<WmSensitive> page = wmSensitiveMapper.selectPage(new Page<>(adSensitiveDto.getPage(), adSensitiveDto.getSize()), wrapper);
        PageResponseResult result = new PageResponseResult((int)page.getCurrent(), (int)page.getSize(), (int)page.getTotal());
        result.setData(page.getRecords());
        return result;
    }

    @Override
    public ResponseResult update(WmSensitive wmSensitive) {
        //校验参数
        if (wmSensitive.getSensitives()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        LambdaQueryWrapper<WmSensitive> wrapper = Wrappers.<WmSensitive>lambdaQuery().eq(WmSensitive::getId, wmSensitive.getId());
        wmSensitiveMapper.update(wmSensitive,wrapper);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult del(Integer id) {
        //校验参数
        if (id==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        wmSensitiveMapper.deleteById(id);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
