package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dto.LoginDto;
import com.heima.user.mapper.ApUserMapper;
import com.heima.model.user.pojo.ApUser;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * APP用户表(ApUser)表服务实现类
 *
 * @author Xujian
 * @since 2023-06-23 17:56:58
 */
@Service("apUserService")
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {
    @Resource
    private ApUserMapper apUserMapper;

    @Override
    public ResponseResult login_auth(LoginDto dto) {
        //1. 判断是正常登陆还是匿名登陆
        if (StringUtils.isNotEmpty(dto.getPhone()) && StringUtils.isNotEmpty(dto.getPassword())) {
            //1.1 如果是正常登录
            //1.1.2 根据手机号查询用户信息
            LambdaQueryWrapper<ApUser> wrapper = Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, dto.getPhone());
            ApUser apUser = apUserMapper.selectOne(wrapper);
            if (apUser == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
            }
            //1.1.3 使用数据库返回的盐+用户输入的密码 加密得到密文
            String hex = MD5Utils.encodeWithSalt(dto.getPassword(),apUser.getSalt());
            //1.1.4 比对数据库的密码和生成的密文, 一致登录成功, 不一致登录失败
            if (!StringUtils.equals(hex, apUser.getPassword())) {
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }

            //1.1.5 封装返回数据
            Map<String, Object> data = new HashMap<>();
            data.put("token", AppJwtUtil.getToken(apUser.getId()));
            apUser.setSalt(null);
            apUser.setPassword(null);
            data.put("user", apUser);

            return ResponseResult.okResult(data);
        } else {
            //1.2 是匿名登录 , 直接使用0生成token返回
            Map<String, Object> data = new HashMap<>();
            data.put("token", AppJwtUtil.getToken(0L));
            return ResponseResult.okResult(data);
        }

    }
}

