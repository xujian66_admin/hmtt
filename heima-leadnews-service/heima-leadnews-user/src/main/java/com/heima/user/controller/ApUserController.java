package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.pojo.ApUser;
import com.heima.user.mapper.ApUserMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/27 15:23
 */
@RestController
@RequestMapping(path = "/api/v1/user")
public class ApUserController {

    @Resource
    private ApUserMapper apUserMapper;

    @GetMapping(path = "/{id}")
    public ResponseResult<ApUser> findById(@PathVariable("id") Long id){
        ApUser apUser = apUserMapper.selectById(id);
        apUser.setPassword(null);
        apUser.setSalt(null);
        return ResponseResult.okResult(apUser);
    }
}
