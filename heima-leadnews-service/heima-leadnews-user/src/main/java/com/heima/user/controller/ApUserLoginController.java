package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dto.LoginDto;
import com.heima.user.service.ApUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xu
 */
@RestController
@RequestMapping(path = "/api/v1/login")
public class ApUserLoginController {

    @Resource
    private ApUserService apUserService;

    @PostMapping(path = "/login_auth")
    public ResponseResult login_auth(@RequestBody LoginDto dto){
        return apUserService.login_auth(dto);
    }
}