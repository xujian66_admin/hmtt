package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dto.LoginDto;
import com.heima.model.user.pojo.ApUser;

/**
 * APP用户表(ApUser)表服务接口
 *
 * @author Xujian
 * @since 2023-06-23 17:56:58
 */
public interface ApUserService extends IService<ApUser> {

    ResponseResult login_auth(LoginDto dto);
}

