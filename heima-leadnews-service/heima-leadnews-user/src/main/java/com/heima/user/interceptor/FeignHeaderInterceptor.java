package com.heima.user.interceptor;

import com.heima.common.thread.UserinfoThreadLocalUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

/**
 * @author xujian
 * @date 2023/6/26 21:47
 */
@Component
public class FeignHeaderInterceptor implements RequestInterceptor {
    /**
     * FeignHeaderInterceptor的引入
     * 使得在feign调用过程中可以获取到线程id
     *
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        Long userId = UserinfoThreadLocalUtil.getUser().getUserId();
        if (userId!=null){
            requestTemplate.header("userId", String.valueOf(userId));
        }
    }
}
