package com.heima.user.controller;



import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dto.AuthDto;
import com.heima.user.service.ApUserRealnameService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * APP实名认证信息表(ApUserRealname)表控制层
 *
 * @author Xujian
 * @since 2023-06-26 16:27:20
 */
@RestController
@RequestMapping(path = "/api/v1/auth")
public class ApUserRealnameController  {
    /**
     * 服务对象
     */
    @Resource
    private ApUserRealnameService apUserRealnameService;
    /**
     * 提交用户实名认证信息
     * 参数名=参数值&参数名=参数值&参数名=参数值
     *
     * @return
     */
    @PostMapping(path = "/submit")
    public ResponseResult submit(MultipartFile font_image,
                                 MultipartFile back_image,
                                 MultipartFile hold_image,
                                 MultipartFile live_image,
                                 String name,
                                 String idno) throws IOException {

        return apUserRealnameService.submit(font_image,back_image,hold_image,live_image,name,idno);
    }

    /**
     * 查询实名列表
     * @param dto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult loadListByStatus(@RequestBody AuthDto dto){
        return apUserRealnameService.loadListByStatus(dto);
    }

    /**
     * 审核通过
     * @param dto
     * @return
     */
    @PostMapping("/authPass")
    public ResponseResult authPass(@RequestBody AuthDto dto){
        dto.setStatus((short) 9);
        return apUserRealnameService.updateAuthStatus(dto);
    }


    @PostMapping(path = "/authFail")
    public ResponseResult authFail(@RequestBody AuthDto dto){
        dto.setStatus((short) 2);
        return apUserRealnameService.updateAuthStatus(dto);
    }

}

