package com.heima.user.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hei.feign.client.ApUserFeignClient;
import com.heima.common.exception.CustomException;
import com.heima.common.thread.UserinfoThreadLocalUtil;
import com.heima.file.MinIOTemplate;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dto.AuthDto;
import com.heima.model.user.pojo.ApUser;
import com.heima.model.wemedia.entity.WmUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.mapper.ApUserRealnameMapper;
import com.heima.model.user.pojo.ApUserRealname;
import com.heima.user.service.ApUserRealnameService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.jar.Pack200;

/**
 * APP实名认证信息表(ApUserRealname)表服务实现类
 *
 * @author Xujian
 * @since 2023-06-26 16:27:25
 */
@Service("apUserRealnameService")
public class ApUserRealnameServiceImpl extends ServiceImpl<ApUserRealnameMapper, ApUserRealname> implements ApUserRealnameService {

    @Resource
    private MinIOTemplate minIOTemplate;
    @Resource
    private ApUserRealnameMapper apUserRealnameMapper;
    @Resource
    private ApUserMapper apUserMapper;
    @Resource
    private ApUserFeignClient apUserFeignClient;

    @Override
    public ResponseResult submit(MultipartFile font_image, MultipartFile back_image, MultipartFile hold_image, MultipartFile live_image, String name, String idno) throws IOException {
        //1. 校验参数
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(idno)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (font_image.isEmpty() || back_image.isEmpty() || hold_image.isEmpty() || live_image.isEmpty()) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "缺失认证信息");
        }
        //2. 上传提交的照片数据到minio
        //2.1 身份证正面上传
        String filename = font_image.getOriginalFilename();
        filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
        String font_url = minIOTemplate.uploadImgFile("heima", filename, font_image.getInputStream());

        //2.2 身份证反面上传
        filename = back_image.getOriginalFilename();
        filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
        String back_url = minIOTemplate.uploadImgFile("heima", filename, back_image.getInputStream());

        //2.3 手持照片上传
        filename = hold_image.getOriginalFilename();
        filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
        String hold_url = minIOTemplate.uploadImgFile("heima", filename, hold_image.getInputStream());

        //2.4 手持照片上传
        filename = live_image.getOriginalFilename();
        filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
        String live_url = minIOTemplate.uploadImgFile("heima", filename, live_image.getInputStream());

        //3. 封装数据保存到数据 --- 构建者模式进行数据封装
        ApUserRealname apUserRealname = ApUserRealname.builder()
                .userId(UserinfoThreadLocalUtil.getUser().getUserId())
                .name(name)
                .idno(idno)
                .fontImage(font_url)
                .backImage(back_url)
                .holdImage(hold_url)
                .liveImage(live_url)
                .status(1)
                .createdTime(new Date())
                .updatedTime(new Date())
                .build();

        apUserRealnameMapper.insert(apUserRealname);

        return ResponseResult.okResult("操作成功");
    }

    @Override
    public ResponseResult loadListByStatus(AuthDto dto) {
        //校验参数
        dto.checkParam();
        //定义wrapper
        LambdaQueryWrapper<ApUserRealname> wrapper = Wrappers.<ApUserRealname>lambdaQuery();
        //判断是否携带状态
        if (dto.getStatus() != null) {
            wrapper.eq(ApUserRealname::getStatus, dto.getStatus());
        }

        Page<ApUserRealname> page = apUserRealnameMapper.selectPage(new Page<>(dto.getPage(), dto.getSize()), wrapper);
        PageResponseResult result = new PageResponseResult((int) page.getCurrent(), (int) page.getSize(), (int) page.getTotal());
        result.setData(page.getRecords());
        return result;


    }

    @GlobalTransactional
    @Override
    public ResponseResult updateAuthStatus(AuthDto dto) {
        //校验参数
        if (dto == null || dto.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getStatus() != 2 && dto.getStatus() != 9) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //修改实名信息、审核中的状态
        ApUserRealname apUserRealname = apUserRealnameMapper.selectById(dto.getId());
        if (apUserRealname==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        apUserRealname.setStatus(dto.getStatus().intValue());
        if (dto.getMsg()!=null){
            apUserRealname.setReason(dto.getMsg());
        }
        apUserRealnameMapper.updateById(apUserRealname);
        if (dto.getStatus()==9) {
            createWmUser(apUserRealname.getId());
        }

        return ResponseResult.okResult("操作成功");

    }

    private void createWmUser(Long id) {
        ApUser apUser = apUserMapper.selectById(id);
        if (apUser==null) {
            throw new CustomException(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        WmUser wmUser=new WmUser();
        BeanUtil.copyProperties(apUser,wmUser,"id");
        wmUser.setAvatar(apUser.getImage());
        wmUser.setNickname(apUser.getNicknme());
        wmUser.setApUserId(apUser.getId());
        wmUser.setStatus(1);
        wmUser.setType(1);
        ResponseResult save = apUserFeignClient.save(wmUser);
        if (save.getCode()!=200) {
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }

    }


}

