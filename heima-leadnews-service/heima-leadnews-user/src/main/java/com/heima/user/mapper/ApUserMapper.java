package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojo.ApUser;

/**
 * APP用户表(ApUser)表数据库访问层
 *
 * @author Xujian
 * @since 2023-06-23 17:56:56
 */
public interface ApUserMapper extends BaseMapper<ApUser> {

}

