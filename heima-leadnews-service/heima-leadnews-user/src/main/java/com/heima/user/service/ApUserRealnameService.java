package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dto.AuthDto;
import com.heima.model.user.pojo.ApUserRealname;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * APP实名认证信息表(ApUserRealname)表服务接口
 *
 * @author Xujian
 * @since 2023-06-26 16:27:25
 */
public interface ApUserRealnameService extends IService<ApUserRealname> {

    ResponseResult submit(MultipartFile font_image, MultipartFile back_image, MultipartFile hold_image, MultipartFile live_image, String name, String idno) throws IOException;

    ResponseResult loadListByStatus(AuthDto dto);


    ResponseResult updateAuthStatus(AuthDto dto);
}

