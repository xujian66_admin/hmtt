package com.hei.feign;

import com.hei.feign.client.ApUserFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.entity.WmUser;
import feign.hystrix.FallbackFactory;

/**
 * @author xujian
 * @date 2023/6/26 19:09
 */
public class ApUserFeignClientFallbackFactory implements FallbackFactory<ApUserFeignClient> {
    @Override
    public ApUserFeignClient create(Throwable throwable) {
        return new ApUserFeignClient() {
            @Override
            public ResponseResult save(WmUser wmUser) {
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
        };
    }
}
