package com.hei.feign;

import com.hei.feign.client.TaskFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.schedule.pojo.TaskInfo;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xujian
 * @date 2023/6/21 13:23
 */
@Slf4j
public class TaskFeignClientFallbackFactory implements FallbackFactory<TaskFeignClient> {
    @Override
    public TaskFeignClient create(Throwable throwable) {
        return new TaskFeignClient() {

            @Override
            public ResponseResult<Long> addTask(TaskDto task) {
                log.error("任务调度服务 , 新增任务失败");
                return ResponseResult.okResult(null);
            }

            @Override
            public ResponseResult cancel(Long taskId) {
                log.error("任务调度服务 , 取消任务失败");
                return ResponseResult.okResult(null);
            }

            @Override
            public ResponseResult<TaskInfo> poll(Integer taskType, Integer priority) {
                log.error("任务调度服务 , 拉取任务失败");
                return ResponseResult.okResult(null);
            }
        };
    }
}
