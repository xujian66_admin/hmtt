package com.hei.feign.config;

import com.hei.feign.ApUserFeignClientFallbackFactory;
import com.hei.feign.ArticleClientFallbackFactory;
import com.hei.feign.TaskFeignClientFallbackFactory;
import com.hei.feign.UserFeignClientFallbackFactory;
import com.hei.feign.client.UserFeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xujian
 * @date 2023/6/15 21:29
 */
@Configuration
public class LeadnewsFeignConfiguration {
    @Bean
    public ArticleClientFallbackFactory articleClientFallbackFactory(){
        return new ArticleClientFallbackFactory();
    }

    @Bean
    public TaskFeignClientFallbackFactory taskFeignClientFallbackFactory(){
        return new TaskFeignClientFallbackFactory();
    }
    @Bean
    public ApUserFeignClientFallbackFactory apUserFeignClientFallbackFactory(){
        return new ApUserFeignClientFallbackFactory();
    }

    @Bean
    public UserFeignClientFallbackFactory userFeignClientFallbackFactory(){
        return new UserFeignClientFallbackFactory();
    }
}
