package com.hei.feign;

import com.hei.feign.client.ApArticleFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dto.ArticleDto;
import feign.hystrix.FallbackFactory;

/**
 * @author xujian
 * @date 2023/6/15 21:32
 */
public class ArticleClientFallbackFactory implements FallbackFactory<ApArticleFeignClient> {
    @Override
    public ApArticleFeignClient create(Throwable throwable) {
        return new ApArticleFeignClient() {
            @Override
            public ResponseResult<String> save(ArticleDto dto) {
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
        };
    }
}
