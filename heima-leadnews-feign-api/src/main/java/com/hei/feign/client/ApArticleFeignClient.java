package com.hei.feign.client;

import com.hei.feign.ArticleClientFallbackFactory;
import com.hei.feign.config.LeadnewsFeignConfiguration;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dto.ArticleDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author xujian
 * @date 2023/6/15 21:26
 */
@FeignClient(value = "leadnews-article",configuration = LeadnewsFeignConfiguration.class,fallbackFactory = ArticleClientFallbackFactory.class)
public interface ApArticleFeignClient {
    @PostMapping(path = "/api/v1/article/save")
    public ResponseResult<String> save(@RequestBody ArticleDto dto);
}
