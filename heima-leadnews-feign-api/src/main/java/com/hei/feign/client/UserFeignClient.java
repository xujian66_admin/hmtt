package com.hei.feign.client;

import com.hei.feign.UserFeignClientFallbackFactory;
import com.hei.feign.config.LeadnewsFeignConfiguration;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.pojo.ApUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author xujian
 * @date 2023/6/27 15:28
 */
@FeignClient(value = "leadnews-user",fallbackFactory = UserFeignClientFallbackFactory.class,configuration = LeadnewsFeignConfiguration.class)
public interface UserFeignClient {
    @GetMapping(path = "/api/v1/user/{id}")
     ResponseResult<ApUser> findById(@PathVariable("id") Long id);
}
