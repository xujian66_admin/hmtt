package com.hei.feign.client;

import com.hei.feign.TaskFeignClientFallbackFactory;
import com.hei.feign.config.LeadnewsFeignConfiguration;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dto.TaskDto;
import com.heima.model.schedule.pojo.TaskInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author xujian
 * @date 2023/6/21 13:21
 */
@FeignClient(value = "leadnews-schedule",fallbackFactory = TaskFeignClientFallbackFactory.class,configuration = LeadnewsFeignConfiguration.class)
public interface TaskFeignClient {
    /**
     * 添加任务
     *
     * @param task
     * @return
     */
    @PostMapping(path = "/api/v1/task/add")
    public ResponseResult<Long> addTask(@RequestBody TaskDto task);

    /**
     * 取消任务
     *
     * @param taskId
     * @return
     */
    @DeleteMapping(path = "/api/v1/task/{taskId}")
    public ResponseResult cancel(@PathVariable("taskId") Long taskId);


    /**
     * 消费任务
     *
     * @return
     */
    @GetMapping(path = "/api/v1/task/poll/{taskType}/{priority}")
    public ResponseResult<TaskInfo> poll(@PathVariable("taskType") Integer taskType, @PathVariable("priority") Integer priority);
}
