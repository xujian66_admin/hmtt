package com.hei.feign.client;

import com.hei.feign.ApUserFeignClientFallbackFactory;
import com.hei.feign.config.LeadnewsFeignConfiguration;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.entity.WmUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author xujian
 * @date 2023/6/26 19:08
 */
@FeignClient(value = "leadnews-wemedia",configuration = LeadnewsFeignConfiguration.class,fallbackFactory = ApUserFeignClientFallbackFactory.class)
public interface ApUserFeignClient {
    /**
     * 保存用户接口
     * @param wmUser
     * @return
     */
    @PostMapping(path = "/api/v1/user/save")
    public ResponseResult save(@RequestBody WmUser wmUser);
}
