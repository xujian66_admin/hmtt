package com.heima;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xujian
 * @date 2023/6/24 16:32
 */
@SpringBootApplication
@MapperScan("com.heima.es.mapper")
public class EsTest {
    public static void main(String[] args) {
        SpringApplication.run(EsTest.class,args);
    }
}
