package com.heima.es.mapper;

import com.heima.es.pojo.SearchArticleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ApArticleDao {


    /**
     * 查询所有的频道列表
     * @return
     */
    List<SearchArticleVo> loadArticleList();

}