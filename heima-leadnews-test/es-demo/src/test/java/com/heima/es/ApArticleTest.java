package com.heima.es;

import com.alibaba.fastjson.JSON;
import com.heima.es.mapper.ApArticleDao;
import com.heima.es.pojo.SearchArticleVo;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ApArticleTest {

    @Resource
    private ApArticleDao apArticleDao;

    @Resource
    private RestHighLevelClient restHighLevelClient;

    /**
     * 注意：数据量的导入，如果数据量过大，需要分页导入
     *
     * @throws Exception
     */
    @Test
    public void init() throws Exception {
        //1. 查询所有的文章数据
        List<SearchArticleVo> searchArticleVos = apArticleDao.loadArticleList();
        BulkRequest request = new BulkRequest();
        searchArticleVos.stream().forEach(searchArticleVo -> {
            searchArticleVo.setSuggestion(Arrays.asList(searchArticleVo.getLabels(),searchArticleVo.getTitle()));
            request.add(new IndexRequest("app_article_info").id(searchArticleVo.getId()+"").source(JSON.toJSONString(searchArticleVo),XContentType.JSON));
        });
        restHighLevelClient.bulk(request,RequestOptions.DEFAULT);
    }


}