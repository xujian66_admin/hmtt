package com.heima.mongodb;

import com.heima.mongodb.pojo.Places;
import com.heima.mongodb.pojo.User;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xujian
 * @date 2023/6/24 11:15
 */
@SpringBootTest
public class MongoTest {
    @Resource
    private MongoTemplate mongoTemplate;

    @Test
    void add() {
        User user1 = new User(ObjectId.get(), "hongqigong", "北丐", "123456", 18);
        User user2 = new User(ObjectId.get(), "ouyangfeng", "欧阳锋", "123456", 20);
        User user3 = new User(ObjectId.get(), "wangchongyang", "王重阳", "123456", 28);
        User user4 = new User(ObjectId.get(), "huangrong", "huangrong", "123456", 30);
        mongoTemplate.save(user1);
        mongoTemplate.save(user2);
        mongoTemplate.save(user3);
        mongoTemplate.save(user4);
    }
    @Test
    void findById() {
        User user = mongoTemplate.findById(new ObjectId("625bedad0b90eb349e5762c0"), User.class);
        System.out.println(user);
    }
    @Test
    void update() {
        User user = mongoTemplate.findById(new ObjectId("625becbbcca3d96b3da4bbbc"), User.class);
        user.setNick_name("神雕大侠");
        mongoTemplate.save(user);
    }
    @Test
    void delete() {
        //User user = mongoTemplate.findById(new ObjectId("625becbbcca3d96b3da4bbbc"), User.class);
        //mongoTemplate.remove(user);

        mongoTemplate.remove(Query.query(Criteria.where("username").is("yangguo")),User.class);
    }

    @Test
    void query(){
         //查询username=yangguo用户信息
        //Query query = Query.query(Criteria.where("username").is("yangguo"));
        //List<User> users = mongoTemplate.find(query, User.class);
        //users.stream().forEach(user -> System.out.println(user));

        //查询年龄在18-30之间的用户信息
        //Query query = Query.query(Criteria.where("age").gt(18).lte(30));
        //List<User> users = mongoTemplate.find(query, User.class);
        //users.stream().forEach(user -> System.out.println(user));

        //查询昵称中包含杨的用户信息
        //Query query = Query.query(Criteria.where("nick_name").regex(".*?北.*"));
        //List<User> users = mongoTemplate.find(query, User.class);
        //users.stream().forEach(user-> System.out.println(user));



    }

    @Test
    void multiQuery(){

        //Criteria criteria=new Criteria();
        //criteria.orOperator(Criteria.where("nick_name").regex(".*?王.*"),Criteria.where("age").gt(20));
        //Query query = Query.query(criteria);
        //List<User> users = mongoTemplate.find(query, User.class);
        //users.stream().forEach(user -> System.out.println(user));
        //查询昵称中包含！或者年龄大于20岁的用户信息 --或者
        Criteria criteria=new Criteria();
        criteria.orOperator(Criteria.where("nick_name").regex(".*?王.*"),Criteria.where("age").gt(20));
        Query query = Query.query(criteria);
        List<User> users = mongoTemplate.find(query, User.class);
        users.stream().forEach(user -> System.out.println(user));

    }
    @Test
    public void testAdd() {

        Places places = new Places();
        places.setId(ObjectId.get());
        places.setAddress("湖北省武汉市东西湖区金山大道");
        places.setTitle("金山大道");
        places.setLocation(new GeoJsonPoint(114.226867, 30.636001));
        mongoTemplate.save(places);

        Places places2 = new Places();
        places2.setId(ObjectId.get());
        places2.setAddress("湖北省武汉市东西湖区奥园东路");
        places2.setTitle("奥园东路");
        places2.setLocation(new GeoJsonPoint(114.240592, 30.650171));
        mongoTemplate.save(places2);

        Places places3 = new Places();
        places3.setId(ObjectId.get());
        places3.setAddress("湖北省武汉市黄陂区X003");
        places3.setTitle("黄陂区X003");
        places3.setLocation(new GeoJsonPoint(114.355876, 30.726886));
        mongoTemplate.save(places3);

        Places places4 = new Places();
        places4.setId(ObjectId.get());
        places4.setAddress("湖北省武汉市黄陂区汉口北大道");
        places4.setTitle("汉口北大道");
        places4.setLocation(new GeoJsonPoint(114.364111, 30.722166));
        mongoTemplate.save(places4);
    }

    //查询附近且获取间距
    @Test
    public void testNear() {
        //1. 构造中心点(圆点)
        GeoJsonPoint point = new GeoJsonPoint(116.404, 39.915);
        //2. 构建NearQuery对象
        NearQuery query = NearQuery.near(point, Metrics.KILOMETERS).maxDistance(100, Metrics.KILOMETERS);

        //3. 调用mongoTemplate的geoNear方法查询
        GeoResults<Places> results = mongoTemplate.geoNear(query, Places.class);
        //4. 解析GeoResult对象，获取距离和数据
        for (GeoResult<Places> result : results) {
            Places places = result.getContent();
            double value = result.getDistance().getValue();
            System.out.println(places+"---距离："+value + "km");
        }
    }
    @Test
    void near(){
        GeoJsonPoint geoJsonPoint = new GeoJsonPoint(114.364111,30.722166);
        NearQuery nearQuery = NearQuery.near(geoJsonPoint, Metrics.KILOMETERS).maxDistance(50, Metrics.KILOMETERS);
        GeoResults<Places> geoResults = mongoTemplate.geoNear(nearQuery, Places.class);
        for (GeoResult<Places> geoResult : geoResults) {
            Places content = geoResult.getContent();
            System.out.println(content);
            Distance distance = geoResult.getDistance();
            System.out.println(distance);
            System.out.println("----------");
        }
    }

}
