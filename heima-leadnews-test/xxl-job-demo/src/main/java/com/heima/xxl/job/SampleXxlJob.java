package com.heima.xxl.job;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author xujian
 * @date 2023/6/30 17:11
 */
@Component
@Slf4j
public class SampleXxlJob {
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("hellojob")
    public void demoJobHandler() throws Exception {
        String jobParam = XxlJobHelper.getJobParam();

        log.info("ding~~ 定时任务执行啦,{}",jobParam);
    }
}
