package com.heima;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import javax.annotation.Resource;

/**
 * @author xujian
 * @date 2023/6/17 10:14
 */
@SpringBootTest
public class KafkaProducerTest {
    @Resource
    KafkaTemplate kafkaTemplate;
    @Test
    public void testSend(){
        kafkaTemplate.send("test.topic","test","hai,kafka!");
    }
}
