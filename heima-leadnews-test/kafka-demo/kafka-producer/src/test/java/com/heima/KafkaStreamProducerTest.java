package com.heima;

import com.alibaba.fastjson.JSON;
import com.heima.pojo.ArticleMessage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xujian
 * @date 2023/6/30 11:07
 */
@SpringBootTest
@Slf4j
public class KafkaStreamProducerTest {

    @Resource
    KafkaTemplate kafkaTemplate;
    @Test
    void testSend5() {
        List<String> strs = new ArrayList<String>();
        strs.add("hello word");
        strs.add("hello kafka");
        strs.add("hello spring kafka");
        strs.add("kafka stream");
        strs.add("spring kafka");

        strs.stream().forEach(s -> {
            kafkaTemplate.send("kafka.stream.topic1", "10001", s);
        });
    }
    @Test
    void testSend6() {
        List<ArticleMessage> strs = new ArrayList<ArticleMessage>();
        ArticleMessage message1 = new ArticleMessage(1498972384605040641l, ArticleMessage.UpdateArticleType.LIKES, 1);
        ArticleMessage message4 = new ArticleMessage(1498972384605040641l, ArticleMessage.UpdateArticleType.LIKES, 1);
        ArticleMessage message7 = new ArticleMessage(1498972384605040641l, ArticleMessage.UpdateArticleType.LIKES, 1);
        ArticleMessage message3 = new ArticleMessage(1498972384605040641l, ArticleMessage.UpdateArticleType.LIKES, -1);
        ArticleMessage message2 = new ArticleMessage(1498972384605040641l, ArticleMessage.UpdateArticleType.VIEWS, 1);
        ArticleMessage message6 = new ArticleMessage(1498973263815045122l, ArticleMessage.UpdateArticleType.COLLECTION, 1);
        ArticleMessage message5 = new ArticleMessage(1498973263815045122l, ArticleMessage.UpdateArticleType.COLLECTION, 1);
        ArticleMessage message8 = new ArticleMessage(1498973263815045122l, ArticleMessage.UpdateArticleType.COLLECTION, 1);
        ArticleMessage message9 = new ArticleMessage(1498972384605040641l, ArticleMessage.UpdateArticleType.COLLECTION, 1);

        strs.add(message1);
        strs.add(message2);
        strs.add(message3);
        strs.add(message4);
        strs.add(message5);
        strs.add(message6);
        strs.add(message7);
        strs.add(message8);
        strs.add(message9);

        strs.stream().forEach(s -> {
            kafkaTemplate.send("hot.article.score.topic" , JSON.toJSONString(s));
        });
    }
}
