package com.heima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Administrator
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleMessage {

    /**
     * 文章ID
     */
    private Long articleId;

    /**
     * 修改文章的字段类型
     */
    private UpdateArticleType type;

    /**
     * 修改数据的增量，可为正负
     */
    private Integer add;


    public enum UpdateArticleType {
        COLLECTION, COMMENT, LIKES, VIEWS;
    }
}