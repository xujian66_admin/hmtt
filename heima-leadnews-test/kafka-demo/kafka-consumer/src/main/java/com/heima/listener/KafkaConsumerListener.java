package com.heima.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author xujian
 * @date 2023/6/17 10:29
 */
//@Component
public class KafkaConsumerListener {
    @KafkaListener(topics = "test.topic",groupId = "test01")
    public void test(ConsumerRecord<String, String> record){
        String key = record.key();
        String value = record.value();
        System.out.println("group1中的消费者接收到消息:" + key + " : " + value);
    }
}
