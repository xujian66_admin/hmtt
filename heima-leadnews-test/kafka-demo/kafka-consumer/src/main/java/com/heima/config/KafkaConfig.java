package com.heima.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

/**
 * @author xujian
 * @date 2023/6/17 10:19
 */
@Configuration
public class KafkaConfig {
    @Bean
    public NewTopic newTopic(){
        return TopicBuilder.name("test.topic").build();
    }

    @Bean
    public NewTopic topic3(){
        return TopicBuilder.name("kafka.stream.topic1").build();
    }
    @Bean
    public NewTopic topic4(){
        return TopicBuilder.name("kafka.stream.topic2").build();
    }
}
