package com.heima.config;

import com.alibaba.fastjson.JSON;
import com.heima.pojo.ArticleMessage;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xujian
 * @date 2023/6/30 21:58
 */
@Configuration
public class StreamConfig {
    public KStream<String, String> kStream(StreamsBuilder builder) {
        KStream<String, String> kStream = builder.stream("hot.article.score.topic");
        kStream
                //JSON转为Java对象
                .mapValues(value -> JSON.parseObject(value, ArticleMessage.class))
                //key和值处理 key文章id value：行为类型：数量
                .map((key, value) -> new KeyValue<>(value.getArticleId(), value.getType().name() + ":" + value.getAdd()))
                //根据可以进行分钟
                .groupByKey(Grouped.with(Serdes.Long(), Serdes.String()))
                //设置时间窗口
                .windowedBy(TimeWindows.of(Duration.ofMillis(10000)))
                //数据聚合
                .aggregate(() -> "COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0", ((key, value, aggregate) -> {
                    //获取历史数据聚合结果（每一种行为对应的数量）使用Map封装
                    String[] behaviors = aggregate.split(",");
                    //对切割之后的每一种用户行为进行处理key：行为名称 value：历史行为数量
                    Map<String, Long> map = Arrays.stream(behaviors).map(behavior -> behavior.split(":"))
                            .collect(Collectors.toMap(behavior -> behavior[0], behavior -> Long.valueOf(behavior[1])));
                    //获取本次数据记录的值和历史聚合结果进行累加
                    String[] values = value.split(":");
                    //4.数量累加计算
                    long count = map.get(values[0]) + Long.valueOf(values[1]);
                    map.put(values[0], count);

                    //将最新的运算结果填充到输出字符串中
                    String format = String.format("COLLECTION:%s,COMMENT:%s,LIKES:%s,VIEWS:%s", map.get("COLLECTION"), map.get("COMMENT"), map.get("LIKES"), map.get("VIEWS"));
                    return format;
                }), Materialized.with(Serdes.Long(), Serdes.String()))
                .toStream()
                .map((key, value) -> new KeyValue<>(key.toString(), value))
                .to("kafka.stream.topic4");

        return kStream;
    }

}
