package com.heima.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author xujian
 * @date 2023/6/30 11:41
 */
@Component
@Slf4j
public class KafkaStreamConsumerListener {
    @KafkaListener(topics = "kafka.stream.topic2",groupId = "stream")
    public void listenTopic1(ConsumerRecord<String,String> record){
        String key =record.key();
        String value=record.value();
        log.info("单词:{}出现了{}次",key,value);

    }
}
