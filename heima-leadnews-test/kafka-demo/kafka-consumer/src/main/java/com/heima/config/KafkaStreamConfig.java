package com.heima.config;

import com.alibaba.fastjson.JSON;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.internals.TimeWindow;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;

/**
 * @author xujian
 * @date 2023/6/30 11:14
 */
//@Configuration
public class KafkaStreamConfig {

    @Bean
    public KStream<String,String> kStream(StreamsBuilder builder){
        //1.定义数据来源
        KStream<String,String> kStream=builder.<String,String>stream("kafka.stream.topic1");
        kStream
                //2.1对原始数据中的value字符串进行切割 mapValues：对流中数据的Value进行处理转化
        .mapValues(value -> value.split(" "))
                //2.2对value数组进行扁平化处理（将多维数组转化为一维数组）flatMapValues：对流中数据的数组格式的value进行处理转化（多维转一维）
        .flatMapValues(value -> Arrays.asList(value))
                //2.3对数据格式进行转化 使用value作为key map：对流中数据的key和value进行处理转化
        .map(((key, value) -> new KeyValue<>(value,value)))
                //2.4对Key进行分组groupByKey：根据key进行分组
        .groupByKey(Grouped.with(Serdes.String(),Serdes.String()))
                //设置聚合时间窗口 在指定时间窗口范围内的数据会进行一次运算 输出运算结果
        .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))
                //求组中每个单词的数量
        .count(Materialized.with(Serdes.String(),Serdes.Long()))
                //将运算结果发送到另一个topic中 toStream：将其他类型的流转化为kStream
        .toStream()
                //再将格式改成与topic转化格式相同
        .map((key, value) -> new KeyValue<>(key.key(),value.toString()))
                //发送到目标topic
        .to("kafka.stream.topic2");
        return kStream;
    }



}
