package com.heima.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtTest {

    @Test
    public void createJwt() {
        //1 准备数据
        Map map = new HashMap();
        map.put("id", 1);
        map.put("mobile", "13800138000");
        //2 使用JWT的工具类生成token
        long now = System.currentTimeMillis();

        //3. 生成jwt令牌
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, "123456")
                .setClaims(map)
                .setExpiration(new Date(now + 60000))
                .compact();

        System.out.println(token);
    }

    @Test
    public void parseJwt() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJtb2JpbGUiOiIxMzgwMDEzODAwMCIsImlkIjoxLCJleHAiOjE2NTUyNjI3OTR9.3k4yH17oFmZNSPzV-dmeGsHMQQ_nbZGsD12NNgVsea5qyI";
        Claims claims = Jwts.parser()
                .setSigningKey("123456")
                .parseClaimsJws(token)
                .getBody();

        Integer id = claims.get("id", Integer.class);
        String mobile = claims.get("mobile", String.class);
        System.out.println(id + "---" + mobile);
    }

}
