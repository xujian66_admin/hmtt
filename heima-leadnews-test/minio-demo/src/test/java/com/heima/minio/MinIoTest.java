package com.heima.minio;

import com.heima.file.MinIOTemplate;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SpringBootTest
public class MinIoTest {
    @Resource
    private MinIOTemplate minIOTemplate;

    @Test
    public void update() throws Exception {
        //1. 创建客户端对象
        MinioClient client = MinioClient.builder()
                .endpoint("http://62.234.45.68:9000/")
                .credentials("minio", "Xujian0616")
                .build();
        //2. 构建参数
        FileInputStream fis = new FileInputStream("C:\\Users\\徐建\\Pictures\\th.jpg");
        PutObjectArgs args = PutObjectArgs.builder()
                .bucket("leadnews")
                .object("th.jpg")
                .contentType("image/jpeg")
                .stream(fis, fis.available(), -1)
                .build();

        //3. 发送请求上传图片
        client.putObject(args);

        System.out.println("http://62.234.45.68:9000/leadnews/th.jpg");
    }
    @Test
    public void test() throws FileNotFoundException {
        InputStream inputStream = new FileInputStream("C:\\Users\\徐建\\Pictures\\th.jpg");
        String imgFile = minIOTemplate.uploadImgFile("", "shaobi.jpg", inputStream);
        System.out.println(imgFile);
    }

}
