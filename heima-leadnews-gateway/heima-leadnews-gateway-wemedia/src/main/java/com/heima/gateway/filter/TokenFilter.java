package com.heima.gateway.filter;

import com.heima.utils.common.AppJwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author xujian
 * @date 2023/6/10 16:32
 */
@Component
@Order(1)
public class TokenFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //1.放行登录请求
        if (request.getURI().getPath().contains("/login/in")) {
            return chain.filter(exchange);
        }
        //3.获取请求中的token
        String token = request.getHeaders().getFirst("token");
        //4.校验token
        //4.1判断token是否为空
        if (StringUtils.isEmpty(token)) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //4.2校验token是否有效

        try {
            Claims claims = AppJwtUtil.getClaimsBody(token);
            int result = AppJwtUtil.verifyToken(claims);
            if (result != 0 && result != -1) {
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
            Long userId = claims.get("id", Long.class);
            ServerHttpRequest httpRequest = request.mutate().headers(httpHeaders -> httpHeaders.add("userId", userId + "")).build();
            ServerWebExchange webExchange = exchange.mutate().request(httpRequest).build();
            return chain.filter(webExchange);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chain.filter(exchange);
    }
}
