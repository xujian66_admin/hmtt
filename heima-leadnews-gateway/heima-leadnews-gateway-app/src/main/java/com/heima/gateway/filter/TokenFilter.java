package com.heima.gateway.filter;

import com.heima.utils.common.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author Administrator
 * @Date 2023/3/6
 **/
@Component
@Slf4j
public class TokenFilter implements GlobalFilter, Ordered {

    /**
     * 请求经过网关会自动执行GlobalFilter中的 filter , 对请求响应进行过滤
     *
     * @param exchange 交换机 , 可以从交换机中获取请求响应数据
     * @param chain    过滤器链 , 放行到下一个过滤器
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1. 获取请求响应对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //2. 获取请求头中的token
        //2.1 放行到不需要拦截的请求
        String path = request.getURI().getPath();
        log.info("当前请求路径:{}", path);
        if (path.contains("/login/login_auth")) {
            return chain.filter(exchange);
        }
        //2.2 获取请求头中的token
        String token = request.getHeaders().getFirst("token");
        if (StringUtils.isEmpty(token)) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //3. 校验token是否有效
        Claims claims = AppJwtUtil.getClaimsBody(token);
        int result = AppJwtUtil.verifyToken(claims);
        //3.1 token无效, 向客户端返回错误信息
        if (result != 0 && result != -1) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //3.2 token有效, 获取token中的userId
        String userId = claims.get("id").toString();
        log.info("获取到当前登录用户id:{}",userId);
        //4. 将userId发送到下游服务中
        //4.1 对请求对象进行重新构建 , 返回新的请求对象
        ServerHttpRequest httpRequest = request
                // 重建 , 重构
                .mutate()
                .headers(httpHeaders -> httpHeaders.add("userId", userId))
                .build();
        //4.2 对交换机进行重新构建, 设置新的请求对象
        ServerWebExchange webExchange = exchange.mutate().request(httpRequest).build();

        return chain.filter(webExchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}