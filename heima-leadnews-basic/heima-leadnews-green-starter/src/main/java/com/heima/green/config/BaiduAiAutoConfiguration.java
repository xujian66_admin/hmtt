package com.heima.green.config;

import com.baidu.aip.contentcensor.AipContentCensor;
import com.heima.green.BaiduTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xujian
 * @date 2023/6/15 11:04
 */
@Configuration
@EnableConfigurationProperties(BaiduAiProperties.class)
@ConditionalOnClass(AipContentCensor.class)
public class BaiduAiAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public AipContentCensor aipContentCensor(BaiduAiProperties baiduAiProperties){
        AipContentCensor client =new AipContentCensor(baiduAiProperties.getAppId(),baiduAiProperties.getApiKey(),baiduAiProperties.getSecretKey());
        //可选项 设置网络连接数
        client.setConnectionTimeoutInMillis(20000);
        client.setSocketTimeoutInMillis(60000);
        return client;
    }
    @Bean
    @ConditionalOnMissingBean
    public BaiduTemplate baiduTemplate(AipContentCensor aipContentCensor){
        return new BaiduTemplate(aipContentCensor);
    }
}
