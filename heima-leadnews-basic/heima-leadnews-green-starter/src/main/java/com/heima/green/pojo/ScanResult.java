package com.heima.green.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author xujian
 * @date 2023/6/15 10:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScanResult {
    private String conclusion;
    private Integer conclusionType;
    private List<String> words;
}
