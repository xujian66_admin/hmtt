package com.heima.green.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xujian
 * @date 2023/6/15 11:06
 */
@Data
@ConfigurationProperties(prefix = "leadnews.baidu")
public class BaiduAiProperties {
    private String appId;
    private String apiKey;
    private String secretKey;
}
