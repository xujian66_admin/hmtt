package com.heima.file.config;

import com.heima.file.MinIOTemplate;
import io.minio.MinioClient;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author Administrator
 */
@Configuration
@EnableConfigurationProperties(MinIoProperties.class)
@ConditionalOnClass(MinioClient.class)
public class MinIoAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(MinIOTemplate.class)
    public MinIOTemplate minIOTemplate(MinioClient minioClient, MinIoProperties minIoProperties) {
        return new MinIOTemplate(minioClient, minIoProperties);
    }

    @Bean
    public MinioClient buildMinioClient(MinIoProperties minIoProperties) {
        return MinioClient.builder()
            	.credentials(minIoProperties.getAccessKey(), minIoProperties.getSecretKey())
              	.endpoint(minIoProperties.getEndpoint())
                .build();
    }
}