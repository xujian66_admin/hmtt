package com.heima.file.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xujian
 * @date 2023/6/11 14:05
 */
@Data
@ConfigurationProperties(prefix = "leadnews.minio")
public class MinIoProperties {

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String endpoint;
    private String readPath;
}